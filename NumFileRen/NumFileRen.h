// NumFileRen.h : main header file for the NUMFILEREN application
//

#if !defined(AFX_NUMFILEREN_H__3954D2B0_936A_445D_96A0_B50BB5EB887A__INCLUDED_)
#define AFX_NUMFILEREN_H__3954D2B0_936A_445D_96A0_B50BB5EB887A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNumFileRenApp:
// See NumFileRen.cpp for the implementation of this class
//

class CNumFileRenApp : public CWinApp
{
public:
	CNumFileRenApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNumFileRenApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNumFileRenApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NUMFILEREN_H__3954D2B0_936A_445D_96A0_B50BB5EB887A__INCLUDED_)
