; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNumFileRenDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "NumFileRen.h"

ClassCount=3
Class1=CNumFileRenApp
Class2=CNumFileRenDlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_NUMFILEREN_DIALOG
Resource2=IDR_MAINFRAME
Resource3=IDD_ABOUTBOX
Resource4=IDD_SELECTDIR_DIALOG

[CLS:CNumFileRenApp]
Type=0
HeaderFile=NumFileRen.h
ImplementationFile=NumFileRen.cpp
Filter=N

[CLS:CNumFileRenDlg]
Type=0
HeaderFile=NumFileRenDlg.h
ImplementationFile=NumFileRenDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_EDIT_CHARLIMIT

[CLS:CAboutDlg]
Type=0
HeaderFile=NumFileRenDlg.h
ImplementationFile=NumFileRenDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_NUMFILEREN_DIALOG]
Type=1
Class=CNumFileRenDlg
ControlCount=8
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETDIR,button,1342242816
Control4=IDC_EDIT1,edit,1350631552
Control5=IDC_BUTTON_GO,button,1342242816
Control6=IDC_EDIT_CHARLIMIT,edit,1350639744
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352

[DLG:IDD_SELECTDIR_DIALOG]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TREE1,SysTreeView32,1350631479
Control4=IDC_EDIT1,edit,1350633600

