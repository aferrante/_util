// NumFileRenDlg.h : header file
//

#if !defined(AFX_NUMFILERENDLG_H__92CF3147_3CEC_4BF9_B80C_459A0855FFB2__INCLUDED_)
#define AFX_NUMFILERENDLG_H__92CF3147_3CEC_4BF9_B80C_459A0855FFB2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNumFileRenDlg dialog

class CNumFileRenDlg : public CDialog
{
// Construction
public:
	CNumFileRenDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNumFileRenDlg)
	enum { IDD = IDD_NUMFILEREN_DIALOG };
	int		m_nChars;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNumFileRenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	char 	m_pszAnalyzeDir[MAX_PATH];


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CNumFileRenDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonSetdir();
	afx_msg void OnButtonGo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NUMFILERENDLG_H__92CF3147_3CEC_4BF9_B80C_459A0855FFB2__INCLUDED_)
