// NumFileRenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NumFileRen.h"
#include "NumFileRenDlg.h"
#include "SelectDirDlg.h"
#include <direct.h>
#include "WinBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNumFileRenDlg dialog

CNumFileRenDlg::CNumFileRenDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNumFileRenDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNumFileRenDlg)
	m_nChars = 7;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNumFileRenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNumFileRenDlg)
	DDX_Text(pDX, IDC_EDIT_CHARLIMIT, m_nChars);
	DDV_MinMaxInt(pDX, m_nChars, 1, 260);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CNumFileRenDlg, CDialog)
	//{{AFX_MSG_MAP(CNumFileRenDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SETDIR, OnButtonSetdir)
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNumFileRenDlg message handlers

BOOL CNumFileRenDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
		// TODO: Add extra initialization here
	FILE* pfLastDir = fopen("lastrendir.cfg", "rb");
	if(pfLastDir)
	{
		fscanf(pfLastDir, "%s", m_pszAnalyzeDir);
		fclose(pfLastDir);
	}
	else
		strcpy(m_pszAnalyzeDir,"C:\\");
	GetDlgItem(IDC_EDIT1)->SetWindowText(m_pszAnalyzeDir);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNumFileRenDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNumFileRenDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNumFileRenDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CNumFileRenDlg::OnButtonSetdir() 
{
	CSelectDirDlg dlg;
//	dlg.m_bDirAsRoot = true;  // dont go back to the drive level
//	dlg.m_bAllDrives = true;  // list all drives, or list only the single drive if false.  no effect if m_bDirAsRoot is true
	dlg.SetInitDir(m_pszAnalyzeDir);

	
	if(dlg.DoModal() == IDOK)
	{
		while(dlg.m_pszCurrentDir[strlen(dlg.m_pszCurrentDir)-1] == '\\') dlg.m_pszCurrentDir[strlen(dlg.m_pszCurrentDir)-1]=0; // remove trailing '\' if any
		GetDlgItem(IDC_EDIT1)->SetWindowText(dlg.m_pszCurrentDir);
		strcpy(m_pszAnalyzeDir,dlg.m_pszCurrentDir);
	}
	
}

void CNumFileRenDlg::OnButtonGo() 
{
	// TODO: Add your control notification handler code here
	if(AfxMessageBox("are you sure?", MB_YESNO)==IDYES)
	{

		if(!(UpdateData(TRUE))) return;
		if(strlen(m_pszAnalyzeDir)<4)  // prevents "C:\" or other drive root.
		{
			AfxMessageBox("invalid path"); return;
		}
		// do the process.
		//first see if the target dir already exists.

		FILE* pfLastDir = fopen("lastrendir.cfg", "wb");
		if(pfLastDir)
		{
			fprintf(pfLastDir, "%s", m_pszAnalyzeDir);
			fclose(pfLastDir);
		}


		//if so, great.  if not, make it.
		WIN32_FIND_DATA wfd;
		HANDLE hfile = NULL;

		char pszTargetDir[MAX_PATH];
		sprintf(pszTargetDir, "%s\\*.*", m_pszAnalyzeDir);

		FILE* fp = NULL;

		if(strlen(pszTargetDir))
		{
			hfile = FindFirstFile( pszTargetDir,  // pointer to name of file to search for 
				&wfd  // pointer to returned information 
			); 

			if(hfile!=INVALID_HANDLE_VALUE)
			{
				CString temp;
				CString filename;
				if(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
				{
					temp = wfd.cFileName;
					filename.Format("%s\\%s", m_pszAnalyzeDir, temp.Right(m_nChars));
					temp.Format("%s\\%s", m_pszAnalyzeDir, wfd.cFileName);
//					AfxMessageBox(filename);

					rename(temp, filename);
				}

				while((FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				))) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
				{
					if(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
					{
						temp = wfd.cFileName;
//						 AfxMessageBox( wfd.cFileName);
						filename.Format("%s\\%s", m_pszAnalyzeDir, temp.Right(m_nChars));
						temp.Format("%s\\%s", m_pszAnalyzeDir, wfd.cFileName);
		//			 AfxMessageBox(filename);
						rename(temp, filename);
					}
				}
				FindClose( hfile); 
			}

		}

 AfxMessageBox("Done.");
	}	
}
