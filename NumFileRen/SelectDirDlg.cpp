// SelectDirDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NumFileRen.h"
#include "SelectDirDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDirDlg dialog


CSelectDirDlg::CSelectDirDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDirDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectDirDlg)
	//}}AFX_DATA_INIT
	m_pszStartDir=NULL;
	strcpy(m_pszCurrentDir, "");

	m_bDirAsRoot = false;  // dont go back to the drive level
	m_bAllDrives = true;  // list all drives, or list only the single drive if false.  no effect if m_bDirAsRoot is true
}


CSelectDirDlg::~CSelectDirDlg()
{
	if(m_pszStartDir) free(m_pszStartDir);
}

void CSelectDirDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectDirDlg)
	DDX_Control(pDX, IDC_TREE1, m_tree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectDirDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectDirDlg)
	ON_WM_TIMER()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, OnSelchangedTree1)
	ON_NOTIFY(TVN_ITEMEXPANDING, IDC_TREE1, OnItemexpandingTree1)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDirDlg message handlers

int CSelectDirDlg::SetInitDir(char* pszStartDir)
{
	if((pszStartDir)&&(strlen(pszStartDir)))
	{
		if(m_pszStartDir) free(m_pszStartDir);
		char* pch = (char*)malloc(strlen(pszStartDir)+1);
		if(pch)
		{
			strcpy(pch, pszStartDir);
			m_pszStartDir = pch;

			strcpy(m_pszCurrentDir, m_pszStartDir);  // on init, the current dir is the selected dir.
		}
	}
	return 0;
}

int CSelectDirDlg::LoadInitDir(char* pszStartDir)
{
	if((pszStartDir)&&(strlen(pszStartDir)))
	{
		while(pszStartDir[strlen(m_pszStartDir)-1] == '\\')
		{
			pszStartDir[strlen(m_pszStartDir)-1] = 0;  //removes trailing '\'
		}
	}
	if((pszStartDir)&&(strlen(pszStartDir)))
	{
		if(m_pszStartDir) free(m_pszStartDir);
		char* pch = (char*)malloc(strlen(pszStartDir)+1);
		if(pch)
		{
			strcpy(pch, pszStartDir);
			m_pszStartDir = pch;

			strcpy(m_pszCurrentDir, m_pszStartDir);  // on init, the current dir is the selected dir.

			// now, clear and load up the tree control
			m_tree.DeleteAllItems( );

			if(m_bDirAsRoot)
			{ 
				//let's just add last subdir
				pch = strrchr(m_pszStartDir, '\\');
				if(pch == NULL)
				{
					pch = m_pszStartDir;
					strcpy(m_pszParentDir, "");
				}
				else
				{ 
					pch++;
					strncpy(m_pszParentDir, m_pszStartDir, pch-m_pszStartDir); 
					m_pszParentDir[pch-m_pszStartDir]=0; //null term
				}

				HTREEITEM hitem = m_tree.InsertItem( pch, NULL );  // inserts at root
				m_tree.SetItemImage( hitem, 0, 0 );

				// put a single sub dir in on any drive, if there is a subdir.

				// remove any trailing '\' in path.
				int x=strlen(m_pszStartDir)-1;
				if(x>0)
				{
					while(m_pszStartDir[x]=='\\')
					{
						m_pszStartDir[x]=0;
						x--;
					}
				}

				char pszLabel[MAX_PATH+1];
				sprintf(pszLabel, "%s\\*.*", m_pszStartDir );

				WIN32_FIND_DATA wfd;
				HANDLE hfile = NULL;

				bool bInserted = false;
				hfile = FindFirstFile( pszLabel,  // pointer to name of file to search for 
					&wfd  // pointer to returned inf 
					);
				if(hfile)
				{
					if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
					{
						if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
						{
							HTREEITEM hh = m_tree.InsertItem( wfd.cFileName, hitem); 
							m_tree.SetItemImage( hh, 0, 0 );
							bInserted = true;
						}
					}

//AfxMessageBox(pszLabel);
					while(
								(FindNextFile( hfile,  // handle to search 
									&wfd  // pointer to structure for data on found file 
									)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
								&&(!bInserted)
								)
					{
						if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
						{
							if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
							{
								HTREEITEM hh = m_tree.InsertItem( wfd.cFileName, hitem); 
								m_tree.SetItemImage( hh, 0, 0 );
								bInserted = true;
							}
						}
					}
				}

				m_tree.Expand(hitem, TVE_EXPAND);  // show any subdirs.
//				m_tree.Select(hitem, TVGN_CARET);  // select the root item
			}
			else  // 		!(m_bDirAsRoot), so back all the way to drive.
			{
				char pszSearchPath[MAX_PATH+1];
				char pszDrives[MAX_PATH+1];
				DWORD buflen = GetLogicalDriveStrings(MAX_PATH, pszDrives);
					// add all drives at root.
				int i=0;
				do
				{
					int x=0;
					while(pszDrives[i]!=NULL)
					{
						pszSearchPath[x]=pszDrives[i];
						i++; x++;
					} 
					// finds first null.
					pszSearchPath[x]=0;  // null term
					//but, remove trailing '\'
					x--;
					while(pszSearchPath[x]=='\\')
					{
						pszSearchPath[x]=0;
						x--;
					}

//			AfxMessageBox(pszSearchPath);
					char pszLabel[MAX_PATH+1];

					if(
						  (m_bAllDrives)
						||((!m_bAllDrives)&&(strnicmp(pszSearchPath, m_pszStartDir, strlen(pszSearchPath))==0))
						)
					{
						HTREEITEM hitem = m_tree.InsertItem( (LPCTSTR)pszSearchPath, TVI_ROOT, TVI_LAST ); // at root.
						m_tree.SetItemImage( hitem, 0, 0 );
					
						// put a single sub dir in on any drive, if there is a subdir.

						sprintf(pszLabel, "%s\\*.*", pszSearchPath );

						WIN32_FIND_DATA wfd;
						HANDLE hfile = NULL;

						bool bInserted = false;
						hfile = FindFirstFile( pszLabel,  // pointer to name of file to search for 
							&wfd  // pointer to returned inf 
							);
						if(hfile)
						{
							if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
							{
								if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
								{
									HTREEITEM hh = m_tree.InsertItem( wfd.cFileName, hitem); 
									m_tree.SetItemImage( hh, 0, 0 );
									bInserted = true;
								}
							}


							while(
										(FindNextFile( hfile,  // handle to search 
											&wfd  // pointer to structure for data on found file 
											)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
										&&(!bInserted)
										)
							{
								if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
								{
									if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
									{
										HTREEITEM hh = m_tree.InsertItem( wfd.cFileName, hitem); 
										m_tree.SetItemImage( hh, 0, 0 );
										bInserted = true;
									}
								}
							}
						}

						if(strnicmp(pszSearchPath, m_pszStartDir, strlen(pszSearchPath))==0)
						{
							// put in stubs for expansion.

							if(hfile)
							{
								while(FindNextFile( hfile,  // handle to search 
								&wfd  // pointer to structure for data on found file 
								)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
								{
									if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
									{
										if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
										{
											HTREEITEM hitemSub = m_tree.InsertItem( wfd.cFileName, hitem ); 
											m_tree.SetItemImage( hitemSub, 0, 0 );
											// have to see if theres a subdir, if so, add a stub item to get the + sign
											sprintf(pszLabel, "%s\\%s\\*.*", pszSearchPath, wfd.cFileName );

			//								AfxMessageBox(pszLabel);

											WIN32_FIND_DATA wfd2;
											HANDLE hfile2 = NULL;

											hfile2 = FindFirstFile( pszLabel,  // pointer to name of file to search for 
												&wfd2  // pointer to returned inf 
												);
											bool bIns = false;
											if(hfile2)
											{
												if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
												{
													if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
													{
														HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
														m_tree.SetItemImage( hh, 0, 0 );

														bIns = true;
													}
												}

												while(
															(FindNextFile( hfile2,  // handle to search 
																&wfd2  // pointer to structure for data on found file 
																)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
															&&(!bIns)
															)
												{
													if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
													{
														if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
														{
															HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
															m_tree.SetItemImage( hh, 0, 0 );
															bIns = true;
														}
													}
												}
											}
										}
									}
								}
							}
	
// now, we expand the path!
							
							//expand all the items in the path.
							// hitem is the drive at this point
							m_tree.Expand(hitem, TVE_EXPAND);  // show any subdirs.

							strcpy(pszSearchPath, m_pszStartDir);
//	AfxMessageBox(pszSearchPath);
							pch = strchr(pszSearchPath, '\\'); // check if any further subdirs in path.
							if(pch)
							{
								pch++; // advance to next
								if (*pch!=0) // if that wasnt just a trailing "\", proceed
								{
									strcpy(pszSearchPath, pch); // here's the rest of the path
									do
									{
										pch = strchr(pszSearchPath, '\\');
										if(pch!=NULL)
										{
											strncpy(pszLabel, pszSearchPath, (pch-pszSearchPath));
											pszLabel[(pch-pszSearchPath)]=0;
		//	AfxMessageBox(pszLabel);

											if(hitem) hitem = m_tree.GetChildItem(hitem);
											if(hitem)
											{
												bool bFound=false;
												while((!bFound)&&(hitem))
												{
													if(m_tree.GetItemText(hitem).CompareNoCase(pszLabel)==0)
													{
														bFound=true;
													}
													else
													{
														hitem = m_tree.GetNextSiblingItem(hitem);
													}
												}
												if(bFound)
												{
													m_tree.Expand(hitem, TVE_EXPAND);  // show any subdirs.
												}
											}

		//									hitem = m_tree.InsertItem( (LPCTSTR)pszLabel, hitem, TVI_LAST );
		//									m_tree.SetItemImage( hitem, 0, 0 );
											pch++;
											if (*pch!=0) // if that wasnt just a trailing "\", proceed
											{
												strcpy(pszSearchPath, pch); // here's the rest of the path
											}
											else pch = NULL;  // break out.
											
										}
										else  // couldnt find a "\"
										{
											// last one.
		//	AfxMessageBox("last");
		//	AfxMessageBox(pszSearchPath);
		//									hitem = m_tree.InsertItem( (LPCTSTR)pszSearchPath, hitem, TVI_LAST );
		//									m_tree.SetItemImage( hitem, 0, 0 );
											if(hitem) hitem = m_tree.GetChildItem(hitem);
											if(hitem)
											{
												bool bFound=false;
												while((!bFound)&&(hitem))
												{
													if(m_tree.GetItemText(hitem).CompareNoCase(pszSearchPath)==0)
													{
														bFound=true;
													}
													else
													{
														hitem = m_tree.GetNextSiblingItem(hitem);
													}
												}
												if(bFound)
												{
													m_tree.Expand(hitem, TVE_EXPAND);  // show any subdirs.
												}
											}
										}
									} while(pch!=NULL);
								}
//							m_tree.Select(hitem, TVGN_CARET);  // select the root item
							}
						}

					}

					i++;  // increments to next string.
					if(pszDrives[i]==NULL) // detected end.
						strcpy(pszSearchPath, "");
				}
				while (strlen(pszSearchPath)>0);

			}

			return 0;
		}
	}
	return -1;
}


// adds the entire subtree, recursively.
int CSelectDirDlg::AddDirEntries(char* pszPath, HTREEITEM htiItem)
{
	WIN32_FIND_DATA wfd;
	HANDLE hfile = NULL;
	char pszSearchPath[MAX_PATH+1];
	char* pch=NULL;

	if(pszPath[strlen(pszPath)-1] == '\\') pszPath[strlen(pszPath)-1]=0; // remove trailing '\' if any

	GetDlgItem(IDC_EDIT1)->SetWindowText(pszPath);
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
	AfxGetApp()->PumpMessage();

	_snprintf(pszSearchPath, MAX_PATH, "%s\\*.*", pszPath);

	pch = strrchr(pszPath, '\\');
	if(pch == NULL) pch = pszPath;
	else pch++;

	HTREEITEM hitem = m_tree.InsertItem( pch, htiItem );  // inserts at root if hti=null.
	m_tree.SetItemImage( hitem, 0, 0 );

	hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
		&wfd  // pointer to returned inf 
		);
	if(hfile)
	{
		if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
		{
			if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
			{
				char pszSubSearchPath[MAX_PATH+1];
				_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszPath, wfd.cFileName);
				
				//AfxMessageBox(pszSubSearchPath);

				AddDirEntries(pszSubSearchPath, hitem);  // to be called only by init or itself
			}
		}

		while(FindNextFile( hfile,  // handle to search 
		&wfd  // pointer to structure for data on found file 
		)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
		{
			if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
			{
				if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
				{
					char pszSubSearchPath[MAX_PATH+1];
					_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszPath, wfd.cFileName);

					//AfxMessageBox(pszSubSearchPath);
					
					AddDirEntries(pszSubSearchPath, hitem);  // to be called only by init or itself
				}
			}
		}
	}
	return 0;

}


BOOL CSelectDirDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	
	CImageList* pci = new CImageList;
//	pci->Create( IDB_BITMAP_DIR, 16, 32, RGB(255,255,255) );
	
	pci->Create(16, 16, ILC_COLOR8, 2, 4);  // have to use ILC_COLOR8 for 256 color images

	CBitmap bitmap;
  bitmap.LoadBitmap(IDB_BITMAP_DIR);
  pci->Add(&bitmap, (COLORREF)0x00ff00);  // theres no green...
  bitmap.DeleteObject();

	m_tree.SetImageList( pci, TVSIL_NORMAL );
//	if(m_tree.GetImageList(TVSIL_NORMAL ) == NULL) AfxMessageBox("no img");

	m_tree.GetWindowRect(m_rectctrl[0]);
	ScreenToClient(m_rectctrl[0]);
	GetDlgItem(IDC_EDIT1)->GetWindowRect(m_rectctrl[1]);
	ScreenToClient(m_rectctrl[1]);
	GetDlgItem(IDCANCEL)->GetWindowRect(m_rectctrl[2]);
	ScreenToClient(m_rectctrl[2]);
	GetDlgItem(IDOK)->GetWindowRect(m_rectctrl[3]);
	ScreenToClient(m_rectctrl[3]);
	GetClientRect(m_rect); // init.

//AfxMessageBox("init");


	if((m_pszStartDir)&&(strlen(m_pszStartDir)))
	{
		char  pszDir[MAX_PATH+1];
		strcpy(pszDir, m_pszStartDir); // this is dumb but m_pszStartDir gets freed so just have to do it.
		LoadInitDir(pszDir);
	}
	else
	{
		SetInitDir("C:");
		LoadInitDir("C:");
	}
//	SetTimer(1, 10, NULL);  // just want the window to be vis first before loading all that in.

/*
	// set the palette for the tree control to display 256 color images:
	HBITMAP hBmp = (HBITMAP)::LoadImage( AfxGetInstanceHandle(),
			(LPCTSTR)IDB_BITMAP_DIR, IMAGE_BITMAP, 0,0, LR_CREATEDIBSECTION );

	CBitmap bmp;
	//bmp.LoadBitmap(IDB_BITMAP_DIR);
	bmp.Attach( hBmp );
	BITMAP bm;
	bmp.GetBitmap( &bm );

	// Create a logical palette for the bitmap
	CPalette pal;
	DIBSECTION ds;
	BITMAPINFOHEADER &bmInfo = ds.dsBmih;
	bmp.GetObject( sizeof(ds), &ds );

	int nColors = bmInfo.biClrUsed ? bmInfo.biClrUsed : 1 << bmInfo.biBitCount;

	// Create a halftone palette if colors > 256. 
	CClientDC dc(GetDlgItem(IDC_TREE1));	
	if( nColors > 256 )
	{	
//AfxMessageBox("pal1");
		pal.CreateHalftonePalette( &dc );
	}
	else
	{
		// Create the palette
//AfxMessageBox("pal2");
		RGBQUAD* pRGB = new RGBQUAD[nColors];
		CDC memDC;
		memDC.CreateCompatibleDC(&dc);

		memDC.SelectObject( &bmp );
		::GetDIBColorTable( memDC, 0, nColors, pRGB );

		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * nColors);
		LOGPALETTE* pLP = (LOGPALETTE*) new BYTE[nSize];

		pLP->palVersion = 0x300;
		pLP->palNumEntries = nColors;

		for( int i=0; i < nColors; i++)
		{
			pLP->palPalEntry[i].peRed = pRGB[i].rgbRed;
			pLP->palPalEntry[i].peGreen = pRGB[i].rgbGreen;
			pLP->palPalEntry[i].peBlue = pRGB[i].rgbBlue;
			pLP->palPalEntry[i].peFlags = 0;
		}

		pal.CreatePalette( pLP );

		delete[] pLP;
		delete[] pRGB;
	}


	// now set the palette
	CPaintDC dc2(GetDlgItem(IDC_TREE1));
//	CPaintDC dc2(this);

//	int x = dc2.GetDeviceCaps(RASTERCAPS);
//	char buf[256]; sprintf(buf, "0x%08x", x);
//AfxMessageBox(buf);

	if( (dc2.GetDeviceCaps(RASTERCAPS)&RC_PALETTE) && pal.m_hObject != NULL )
//	if( pal.m_hObject != NULL )
	{
AfxMessageBox("set");
		dc2.SelectPalette( &pal, FALSE );
		dc2.RealizePalette();

//		dc.SelectPalette( &pal, FALSE );
	}

*/


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectDirDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == 1)
	{
		KillTimer(1);
/*
		char pszSearchPath[MAX_PATH+1];
		char pszDrives[MAX_PATH+1];
		DWORD buflen = GetLogicalDriveStrings(MAX_PATH, pszDrives);
		DWORD i = 0;

		CWaitCursor cw;

		do
		{
			int x=0;
			while(pszDrives[i]!=NULL)
			{
				pszSearchPath[x]=pszDrives[i];
				i++; x++;
			} 
			// finds first null.
			pszSearchPath[x]=0;  // null term

//			AfxMessageBox(pszSearchPath);

			AddDirEntries(pszSearchPath);  // recursively adds dirs by drive.

			i++;  // increments to next string.
			if(pszDrives[i]==NULL) // detected end.
				strcpy(pszSearchPath, "");
		}
		while (strlen(pszSearchPath)>0);


		HTREEITEM hItem = m_tree.GetNextItem( NULL, TVGN_CHILD );
		if(hItem) 
		{
			m_tree.SelectItem( hItem );
			GetDlgItem(IDC_EDIT1)->SetWindowText(m_tree.GetItemText( hItem ));
		}
*/

/*	
// moved to OnInitDialog	
		if((m_pszStartDir)&&(strlen(m_pszStartDir)))
		{
			char  pszDir[MAX_PATH+1];
			strcpy(pszDir, m_pszStartDir); // this is dumb but m_pszStartDir gets freed so just have to do it.
			LoadInitDir(pszDir);
		}
		else
		{
			LoadInitDir("C:");
		}
*/
	}
	else
	
	CDialog::OnTimer(nIDEvent);
}

void CSelectDirDlg::OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	HTREEITEM hItem = m_tree.GetSelectedItem( );

//	if(pNMTreeView->itemNew.hItem) 
	if(hItem) 
	{
		// need to assemble path
//		m_tree.SetItem( hItem, TVIF_STATE, (LPCTSTR)NULL,0,0, 
//			TVIS_SELECTED, TVIS_SELECTED, 0);

		char pszSearchPath[MAX_PATH+1];
		// need to get the items full path.
		// assemble it live
		char pszChildPath[MAX_PATH+1]; // path so far.


/*
		if(pNMTreeView->itemOld.hItem) AfxMessageBox(m_tree.GetItemText(pNMTreeView->itemOld.hItem));
		if(pNMTreeView->itemNew.hItem) AfxMessageBox(m_tree.GetItemText(pNMTreeView->itemNew.hItem));

		sprintf(pszSearchPath, "from %d", pNMTreeView->hdr.idFrom);
		AfxMessageBox(pszSearchPath);
*/

		HTREEITEM hitemChild = hItem;//pNMTreeView->itemNew.hItem;
		HTREEITEM hitem = NULL;
		strcpy(pszSearchPath,"");
		sprintf(pszChildPath, "%s", m_tree.GetItemText(hitemChild));
		strcpy(pszSearchPath, pszChildPath);

		do
		{
			hitem = m_tree.GetParentItem(hitemChild);
			if(hitem)
			{
				sprintf(pszSearchPath, "%s\\%s", m_tree.GetItemText(hitem), pszChildPath);
				hitemChild = hitem;
				strcpy(pszChildPath, pszSearchPath);
			}
		} while(hitem!=NULL);


		if(m_bDirAsRoot)
		{
			//just add the thing to the thing.
			sprintf(pszSearchPath, "%s%s", m_pszParentDir, pszChildPath);
		}
	

		strcpy(m_pszCurrentDir, pszSearchPath);
		GetDlgItem(IDC_EDIT1)->SetWindowText(pszSearchPath);
//		m_tree.Invalidate();
	}
	
	*pResult = 0;
}

void CSelectDirDlg::OnItemexpandingTree1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	if(pNMTreeView->hdr.code == TVN_ITEMEXPANDING)
	{
		// expanding or contracting.
		bool bExpand = false;
		
		if(pNMTreeView->action&TVE_COLLAPSE)
		{
			bExpand = false;
		}
		else
		if (pNMTreeView->action&TVE_EXPAND) // contract
		{
			bExpand = true;
		}
		else  // must be toggle TVE_TOGGLE
		{
			// have to detect.
			if((pNMTreeView->itemNew.mask&TVIF_STATE)&&(pNMTreeView->itemNew.state&TVIS_EXPANDED))
			{
				// expanding
				bExpand = true;
			}
			else
			{
				// collapsing
				bExpand = false;
			}
		}

//		m_tree.SetItem( pNMTreeView->itemNew.hItem, TVIF_STATE, (LPCTSTR)NULL,0,0, 
//			TVIS_SELECTED, TVIS_SELECTED, 0);
		m_tree.SelectItem(pNMTreeView->itemNew.hItem);


/*
		if(bExpand)
	AfxMessageBox("expanding");
		else
	AfxMessageBox("contracting");
*/
		//OK, now we have determined that the item is collapsing or expanding.

		if(bExpand)
		{
			m_tree.SetItemImage( pNMTreeView->itemNew.hItem, 1, 1 );

			// means find all children and add.
			char pszSearchPath[MAX_PATH+1];
			// need to get the items full path.
			// assemble it live
			char pszChildPath[MAX_PATH+1]; // path so far.


/*
			if(pNMTreeView->itemOld.hItem) AfxMessageBox(m_tree.GetItemText(pNMTreeView->itemOld.hItem));
			if(pNMTreeView->itemNew.hItem) AfxMessageBox(m_tree.GetItemText(pNMTreeView->itemNew.hItem));

			sprintf(pszSearchPath, "from %d", pNMTreeView->hdr.idFrom);
			AfxMessageBox(pszSearchPath);
*/


			HTREEITEM hitemChild = pNMTreeView->itemNew.hItem;
			HTREEITEM hitem = NULL;
			strcpy(pszSearchPath,"");
			sprintf(pszChildPath, "%s", m_tree.GetItemText(hitemChild));
			strcpy(pszSearchPath, pszChildPath);

			do
			{
				hitem = m_tree.GetParentItem(hitemChild);
				if(hitem)
				{
					sprintf(pszSearchPath, "%s\\%s", m_tree.GetItemText(hitem), pszChildPath);
					hitemChild = hitem;
					strcpy(pszChildPath, pszSearchPath);
				}
			} while(hitem!=NULL);


			if(m_bDirAsRoot)
			{
				//just add the thing to the thing.
				sprintf(pszSearchPath, "%s%s", m_pszParentDir, pszChildPath);
			}


			// means, remove all children. (reset)
			HTREEITEM hItem = NULL;
			do
			{
//	AfxMessageBox(m_tree.GetItemText(pNMTreeView->itemNew.hItem));
				hItem = m_tree.GetNextItem( pNMTreeView->itemNew.hItem, TVGN_CHILD );
				if(hItem) m_tree.DeleteItem(hItem);
			} while (hItem!=NULL);



			if(pszSearchPath[strlen(pszSearchPath)-1] == '\\') pszSearchPath[strlen(pszSearchPath)-1]=0; // remove trailing '\' if any

			GetDlgItem(IDC_EDIT1)->SetWindowText(pszSearchPath);

			char pszLabel[MAX_PATH+1];
			sprintf(pszLabel, "%s\\*.*", pszSearchPath);
//			strncat(pszSearchPath, "\\*.*", min(4, MAX_PATH-strlen(pszSearchPath)));

			WIN32_FIND_DATA wfd;
			HANDLE hfile = NULL;

			hfile = FindFirstFile( pszLabel,  // pointer to name of file to search for 
				&wfd  // pointer to returned inf 
				);
			if(hfile)
			{

				if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
				{
					if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
					{
						HTREEITEM hitemSub = m_tree.InsertItem( wfd.cFileName, pNMTreeView->itemNew.hItem );  // inserts at root if hti=null.
						m_tree.SetItemImage( hitemSub, 0, 0 );

						// have to see if theres a subdir, if so, add a stub item to get the + sign
						sprintf(pszLabel, "%s\\%s\\*.*", pszSearchPath, wfd.cFileName );

	//							AfxMessageBox(pszLabel);

						WIN32_FIND_DATA wfd2;
						HANDLE hfile2 = NULL;

						hfile2 = FindFirstFile( pszLabel,  // pointer to name of file to search for 
							&wfd2  // pointer to returned inf 
							);
						bool bIns = false;
						if(hfile2)
						{
							if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
							{
								if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
								{
									HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
									m_tree.SetItemImage( hh, 0, 0 );
									bIns = true;
								}
							}

							while(
										(FindNextFile( hfile2,  // handle to search 
											&wfd2  // pointer to structure for data on found file 
											)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
										&&(!bIns)
										)
							{
								if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
								{
									if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
									{
										HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
										m_tree.SetItemImage( hh, 0, 0 );
										bIns = true;
									}
								}
							}
						}
					}
				}

				while(FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
				{
					if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
					{
						if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a subdir
						{
							HTREEITEM hitemSub = m_tree.InsertItem( wfd.cFileName, pNMTreeView->itemNew.hItem );  // inserts at root if hti=null.
							m_tree.SetItemImage( hitemSub, 0, 0 );

							// have to see if theres a subdir, if so, add a stub item to get the + sign
							sprintf(pszLabel, "%s\\%s\\*.*", pszSearchPath, wfd.cFileName );

	//								AfxMessageBox(pszLabel);

							WIN32_FIND_DATA wfd2;
							HANDLE hfile2 = NULL;

							hfile2 = FindFirstFile( pszLabel,  // pointer to name of file to search for 
								&wfd2  // pointer to returned inf 
								);
							bool bIns = false;
							if(hfile2)
							{
								if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
								{
									if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
									{
										HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
										m_tree.SetItemImage( hh, 0, 0 );
										bIns = true;
									}
								}

								while(
											(FindNextFile( hfile2,  // handle to search 
												&wfd2  // pointer to structure for data on found file 
												)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
											&&(!bIns)
											)
								{
									if((wfd2.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(!(wfd2.dwFileAttributes&FILE_ATTRIBUTE_HIDDEN)))
									{
										if((strcmp(wfd2.cFileName, "."))&&(strcmp(wfd2.cFileName, ".."))) // only if its a subdir
										{
											HTREEITEM hh = m_tree.InsertItem( wfd2.cFileName, hitemSub); 
											m_tree.SetItemImage( hh, 0, 0 );
											bIns = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		else // collapse
		{
			m_tree.SetItemImage( pNMTreeView->itemNew.hItem, 0, 0 );

/*
			// means, collapse and remove all children.
			HTREEITEM hItem = NULL;
			do
			{
				hItem = m_tree.GetNextItem( pNMTreeView->itemOld.hItem, TVGN_CHILD );
				if(hItem) m_tree.DeleteItem(hItem);
			} while (hItem!=NULL);
*/
		}
//		m_tree.Invalidate();
	}
	*pResult = 0;
}

void CSelectDirDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	CWnd* pwnd = NULL;
	int dx=m_rect.Width()-cx, dy=m_rect.Height()-cy;
	
	m_tree.SetWindowPos(0, 0, 0, 
		m_rectctrl[0].Width()-dx,
		m_rectctrl[0].Height()-dy,
		SWP_NOZORDER|SWP_NOMOVE
		);
//AfxMessageBox("X1");
	pwnd = GetDlgItem(IDC_EDIT1);
	if(pwnd) pwnd->SetWindowPos(0, 
		m_rectctrl[1].left, 
		m_rectctrl[1].top-dy, 
		m_rectctrl[1].Width()-dx,
		m_rectctrl[1].Height(),
		SWP_NOZORDER
		);
//AfxMessageBox("X2");
	pwnd = GetDlgItem(IDCANCEL);
	if(pwnd) pwnd->SetWindowPos(0, 
		m_rectctrl[2].left-dx/2, 
		m_rectctrl[2].top-dy, 
		0,0,
		SWP_NOZORDER|SWP_NOSIZE
		);
//AfxMessageBox("X3");
	pwnd = GetDlgItem(IDOK);
	if(pwnd) pwnd->SetWindowPos(0, 
		m_rectctrl[3].left-dx/2, 
		m_rectctrl[3].top-dy, 
		0,0,
		SWP_NOZORDER|SWP_NOSIZE
		);
	
//AfxMessageBox("X4");

	Invalidate();
/*
	m_tree.Invalidate();
	pwnd = GetDlgItem(IDC_EDIT1);
	if(pwnd) pwnd->Invalidate();
	pwnd = GetDlgItem(IDCANCEL);
	if(pwnd) pwnd->Invalidate();
	pwnd = GetDlgItem(IDOK);
	if(pwnd) pwnd->Invalidate();
*/
}
