// ControlUtilDlg.h : header file
//

#if !defined(AFX_CONTROLUTILDLG_H__07CD8958_C145_448C_B21A_ADC37E89AF02__INCLUDED_)
#define AFX_CONTROLUTILDLG_H__07CD8958_C145_448C_B21A_ADC37E89AF02__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../Common/MFC/EditTC/EditTC.h"

/////////////////////////////////////////////////////////////////////////////
// CControlUtilDlg dialog

class CControlUtilDlg : public CDialog
{
// Construction
public:
	CControlUtilDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CControlUtilDlg)
	enum { IDD = IDD_CONTROLUTIL_DIALOG };
	int		m_nCtrl;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlUtilDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	CEditTC m_editTC;

	// Generated message map functions
	//{{AFX_MSG(CControlUtilDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnChangeEdit1();
	afx_msg void OnChangeRichedit1();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnButtonSettime();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLUTILDLG_H__07CD8958_C145_448C_B21A_ADC37E89AF02__INCLUDED_)
