// GuidGen.h : main header file for the GUIDGEN application
//

#if !defined(AFX_GUIDGEN_H__BBD36986_600A_48F2_879C_85E53493018E__INCLUDED_)
#define AFX_GUIDGEN_H__BBD36986_600A_48F2_879C_85E53493018E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CGuidGenApp:
// See GuidGen.cpp for the implementation of this class
//

class CGuidGenApp : public CWinApp
{
public:
	CGuidGenApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGuidGenApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGuidGenApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GUIDGEN_H__BBD36986_600A_48F2_879C_85E53493018E__INCLUDED_)
