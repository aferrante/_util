// GuidGenDlg.h : header file
//

#if !defined(AFX_GUIDGENDLG_H__3857F3C2_2E8D_4AD8_8ACB_D25175CC497C__INCLUDED_)
#define AFX_GUIDGENDLG_H__3857F3C2_2E8D_4AD8_8ACB_D25175CC497C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CGuidGenDlg dialog

class CGuidGenDlg : public CDialog
{
// Construction
public:
	CGuidGenDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CGuidGenDlg)
	enum { IDD = IDD_GUIDGEN_DIALOG };
	CString	m_szGUID;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGuidGenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CGuidGenDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GUIDGENDLG_H__3857F3C2_2E8D_4AD8_8ACB_D25175CC497C__INCLUDED_)
