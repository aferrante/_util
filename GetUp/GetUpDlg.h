// GetUpDlg.h : header file
//

#if !defined(AFX_GETUPDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_)
#define AFX_GETUPDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../../Common/IMG/BMP/CBmpUtil_MFC.h" 

/////////////////////////////////////////////////////////////////////////////
// CGetUpDlg dialog

class CGetUpDlg : public CDialog
{
// Construction
public:
	CGetUpDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CGetUpDlg)
	enum { IDD = IDD_GETUP_DIALOG };
	int		m_nSeconds;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGetUpDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

	CString m_szFile;
	bool m_bRepeat;
	bool m_bCounting;
	int m_nTick;
	int m_nExpire;
	int m_nFlash;
	CBmpUtil m_bmpu;
	HBITMAP m_hbmp[2];

protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CGetUpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCheckPushpin();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonGo();
	afx_msg void OnButtonSound();
	afx_msg void OnCheckRepeat();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonPlay();
	afx_msg void OnButtonStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETUPDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_)
