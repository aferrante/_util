// GetUpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GetUp.h"
#include "GetUpDlg.h"
#include <sys\timeb.h>
#include <time.h>
#include "..\..\Common\TXT\BufferUtil.h"
#pragma comment(lib, "winmm.lib")
#include <mmsystem.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CGetUpApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
/*
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

*/


/////////////////////////////////////////////////////////////////////////////
// CGetUpDlg dialog

CGetUpDlg::CGetUpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGetUpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGetUpDlg)
	m_nSeconds = 1800;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_szFile = "";
	m_bRepeat = false;
	m_bCounting = false;
}

void CGetUpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGetUpDlg)
	DDX_Text(pDX, IDC_EDIT_SECONDS, m_nSeconds);
	DDV_MinMaxInt(pDX, m_nSeconds, 0, 86400);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGetUpDlg, CDialog)
	//{{AFX_MSG_MAP(CGetUpDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CHECK_PUSHPIN, OnCheckPushpin)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	ON_BN_CLICKED(IDC_BUTTON_SOUND, OnButtonSound)
	ON_BN_CLICKED(IDC_CHECK_REPEAT, OnCheckRepeat)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_PLAY, OnButtonPlay)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetUpDlg message handlers

BOOL CGetUpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.
/*
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
*/

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	CBitmap bmp;
  bmp.LoadBitmap(IDB_BITMAP_PUSHIN);
	m_hbmp[0] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PUSHOUT);
	m_hbmp[1] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();


	((CButton*)GetDlgItem(IDC_CHECK_PUSHPIN))->SetBitmap(m_hbmp[1]);



	// TODO: Add extra initialization here
//	GetDlgItem(IDC_EDIT_MSDUR)->EnableWindow(FALSE);
	m_szFile = theApp.GetProfileString(_T("Alert"), _T("Sound"),   "");      
	m_nSeconds = theApp.GetProfileInt(_T("Alert"), _T("Interval"),    1800);  
	UpdateData(FALSE);
	if(	m_szFile.GetLength()<=0) GetDlgItem(IDC_BUTTON_PLAY)->EnableWindow(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGetUpDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
/*
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
*/
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGetUpDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}

}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGetUpDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CGetUpDlg::OnCheckPushpin() 
{
	// TODO: Add your control notification handler code here
	if(((CButton*)GetDlgItem(IDC_CHECK_PUSHPIN))->GetCheck())
	{
		((CButton*)GetDlgItem(IDC_CHECK_PUSHPIN))->SetBitmap(m_hbmp[0]);
		SetWindowPos(	&wndTopMost, 0,0,0,0, SWP_NOMOVE|SWP_NOSIZE);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CHECK_PUSHPIN))->SetBitmap(m_hbmp[1]);
		SetWindowPos(	&wndNoTopMost, 0,0,0,0, SWP_NOMOVE|SWP_NOSIZE);
	}
}


BOOL CGetUpDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CGetUpDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	int l = theApp.GetProfileInt(_T("Position"), _T("Left"),   -1);  
	int t = theApp.GetProfileInt(_T("Position"), _T("Top"),    -1);  
	theApp.WriteProfileInt(_T("Position"), _T("Left"),   l);      
	theApp.WriteProfileInt(_T("Position"), _T("Top"),    t);      
	if((l>=0)&&(t>=0))
	{
		//check off screen
		if (l>=GetSystemMetrics(SM_CXSCREEN)) { l=0;}
		if (t>=GetSystemMetrics(SM_CYSCREEN)) { t=0;}
		SetWindowPos(	&wndTop, l,t,0,0, SWP_NOZORDER|SWP_NOSIZE);
	}
	
}

void CGetUpDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	WINDOWPLACEMENT wpl;
	GetWindowPlacement(&wpl);

	theApp.WriteProfileInt(_T("Position"), _T("Left"),   wpl.rcNormalPosition.left);      
	theApp.WriteProfileInt(_T("Position"), _T("Top"),    wpl.rcNormalPosition.top);      
	theApp.WriteProfileString(_T("Alert"), _T("Sound"),   m_szFile);      
	theApp.WriteProfileInt(_T("Alert"), _T("Interval"),    m_nSeconds);  

	CDialog::OnCancel();
}

void CGetUpDlg::OnOK() 
{
//	OnButtonGo();	
	
//	CDialog::OnOK();
}

void CGetUpDlg::OnButtonGo() 
{
	// TODO: Add your control notification handler code here
	if(m_bCounting)
	{
		UpdateData(FALSE); //resets the counter
		GetDlgItem(IDC_STATIC_SFRM)->SetWindowText("seconds from");
		GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Now");

		m_bCounting = false;
		KillTimer(27);
		KillTimer(666);
	}
	else
	{
		if(!UpdateData()) return;

		GetDlgItem(IDC_STATIC_SFRM)->SetWindowText("seconds");
		GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Cancel");

		m_bCounting = true;
		m_nTick = clock();
		m_nExpire = m_nTick+(m_nSeconds*1000);
		SetTimer(27,1000, NULL);
	}
	m_nFlash = 0;
	
}

void CGetUpDlg::OnButtonSound() 
{
	int nDlgReturn = IDOK;

	static char BASED_CODE szFilter[] = "Wave files (*.wav)|*.wav|All Files (*.*)|*.*||";
	CFileDialog dlg(TRUE, "wav", NULL, OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_LONGNAMES|OFN_PATHMUSTEXIST|OFN_NOCHANGEDIR, szFilter);
	

	char title[256];
	char path[MAX_PATH];
	sprintf(title, "Select wave file");
	dlg.m_ofn.lpstrTitle = title;
	if(m_szFile.GetLength())
	{
		sprintf(path, "%s", m_szFile);
		dlg.m_ofn.lpstrFile = path;
	}
//	else
//		dlg.m_ofn.lpstrFile = NULL;


	nDlgReturn = dlg.DoModal();
//	m_szFile.ReleaseBuffer(0);

	if(nDlgReturn==IDOK)
	{
		m_szFile = dlg.GetPathName( );
//		AfxMessageBox(m_szFile);

		if(	m_szFile.GetLength() <= 0 )
		{
			GetDlgItem(IDC_BUTTON_PLAY)->EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_BUTTON_PLAY)->EnableWindow(TRUE);
		}
	}
	
}

void CGetUpDlg::OnCheckRepeat() 
{
	if(((CButton*)GetDlgItem(IDC_CHECK_REPEAT))->GetCheck())
		m_bRepeat = true;
	else
		m_bRepeat = false;

	
}

void CGetUpDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==27)
	{
		CString szSeconds;
		szSeconds.Format("%d", (m_nExpire - clock())/1000);

		GetDlgItem(IDC_EDIT_SECONDS)->SetWindowText(szSeconds);
		if(clock()>m_nExpire)
		{
			KillTimer(27);
			OnButtonGo();
			m_nFlash = 0;
			SetTimer(666, 500, NULL);

			if(m_szFile.GetLength()) PlaySound(m_szFile, NULL, SND_ASYNC|SND_FILENAME);

			UpdateData(FALSE);  // seconds.

			if(m_bRepeat)
			{
				OnButtonGo();
			}
		}
	}
	else
	if(nIDEvent==666)
	{
		m_nFlash++;
		FlashWindow(TRUE);

		if(m_nFlash> min((m_nSeconds/2)*2, 120))
		{
			KillTimer(666);
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CGetUpDlg::OnButtonPlay() 
{
	if(m_szFile.GetLength()) PlaySound(m_szFile, NULL, SND_ASYNC|SND_FILENAME);
}

void CGetUpDlg::OnButtonStop() 
{
	PlaySound(NULL, NULL, NULL);
	KillTimer(666);
}
