//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GetUp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GETUP_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_PUSHOUT              129
#define IDB_BITMAP_PUSHIN               130
#define IDC_BUTTON_NOW                  1000
#define IDC_EDIT_UNIX                   1001
#define IDC_CHECK_PUSHPIN               1002
#define IDC_RADIO_FRAMES                1003
#define IDC_RADIO_MS                    1004
#define IDC_EDIT_MSDUR                  1005
#define IDC_STATIC_PLUS                 1006
#define IDC_EDIT_UTC                    1007
#define IDC_EDIT_LOCAL                  1008
#define IDC_BUTTON_GO                   1009
#define IDC_EDIT_FPS                    1010
#define IDC_STATIC_MS                   1011
#define IDC_EDIT_SECONDS                1013
#define IDC_CHECK_REPEAT                1015
#define IDC_BUTTON_SOUND                1016
#define IDC_STATIC_SFRM                 1017
#define IDC_BUTTON_PLAY                 1018
#define IDC_BUTTON_STOP                 1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
