; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CGetUpDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "GetUp.h"

ClassCount=3
Class1=CGetUpApp
Class2=CGetUpDlg
Class3=CAboutDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Resource2=IDD_GETUP_DIALOG

[CLS:CGetUpApp]
Type=0
HeaderFile=GetUp.h
ImplementationFile=GetUp.cpp
Filter=N

[CLS:CGetUpDlg]
Type=0
HeaderFile=GetUpDlg.h
ImplementationFile=GetUpDlg.cpp
Filter=D
LastObject=IDC_BUTTON_STOP
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=GetUpDlg.h
ImplementationFile=GetUpDlg.cpp
Filter=D

[DLG:IDD_GETUP_DIALOG]
Type=1
Class=CGetUpDlg
ControlCount=11
Control1=IDC_CHECK_PUSHPIN,button,1342247043
Control2=IDOK,button,1073741825
Control3=IDCANCEL,button,1073741824
Control4=IDC_BUTTON_GO,button,1342242816
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC_SFRM,static,1342308352
Control7=IDC_EDIT_SECONDS,edit,1350631552
Control8=IDC_CHECK_REPEAT,button,1342242819
Control9=IDC_BUTTON_SOUND,button,1342242816
Control10=IDC_BUTTON_PLAY,button,1342242816
Control11=IDC_BUTTON_STOP,button,1342242816

