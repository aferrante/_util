// FileSortDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FileSort.h"
#include "FileSortDlg.h"
#include "process.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern void DirChangeThread(void* pvArgs);
// CDirUtil* g_pdir = NULL;

typedef struct DirData_t
{
	char* pszPath;
	CDirUtil* pDirUtilObj;
	unsigned long ulOptions;
	CEvent* pEvent;
} DirData_t;



void ScanThread(void* pvArgs);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSortDlg dialog

CFileSortDlg::CFileSortDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileSortDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileSortDlg)
	m_szComp = _T("X:\\Old");
	m_szScan = _T("X:\\New");
	m_bPurge = TRUE;
	m_bCopy = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bRunning = FALSE;

}

void CFileSortDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileSortDlg)
	DDX_Control(pDX, IDC_LIST1, m_lc);
	DDX_Text(pDX, IDC_EDIT_COMPDIR, m_szComp);
	DDX_Text(pDX, IDC_EDIT_SCANDIR, m_szScan);
	DDX_Check(pDX, IDC_CHECK_PURGE, m_bPurge);
	DDX_Check(pDX, IDC_CHECK_COPY, m_bCopy);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileSortDlg, CDialog)
	//{{AFX_MSG_MAP(CFileSortDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSortDlg message handlers

BOOL CFileSortDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	CRect rc; 
	m_lc.GetWindowRect(&rc);
	m_lc.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-16, 0 );

	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFileSortDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFileSortDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFileSortDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CFileSortDlg::OnButtonGo() 
{
	// TODO: Add your control notification handler code here
	if(m_bRunning)
	{
		if(AfxMessageBox("Do you really want to kill the task?", MB_YESNO) == IDYES)
		{
			m_bRunning = FALSE;
			GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Start");
		}
	}
	else
	{
		UpdateData(TRUE);

		if(_beginthread( ScanThread, 0, (void*)(this) ) != -1)
		{
			GetDlgItem(IDC_EDIT_SCANDIR)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_COMPDIR)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_COPY)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_PURGE)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Stop");
		}

	}
}


void ScanThread(void* pvArgs)
{
	CFileSortDlg* pDlg = (CFileSortDlg*)pvArgs;
	unsigned long i=0;
	unsigned long p=0;
	unsigned long a=0;


	pDlg->m_bRunning = TRUE;

	CString szLC;
	int nCount = pDlg->m_lc.GetItemCount();

	szLC.Format("Scanning directory %s", pDlg->m_szScan);
	pDlg->m_lc.InsertItem(nCount, szLC);
	pDlg->m_lc.EnsureVisible(nCount, FALSE);

	pDlg->m_duScan.SetWatchFolder( pDlg->m_szScan.GetBuffer(0));
/*
	EnterCriticalSection (&pDlg->m_duScan.m_crit);

	if(pDlg->m_duScan.m_ppDirChange)
	{
		unsigned long i = 0;
		for(i=0; i<pDlg->m_duScan.m_ulNumDirChanges; i++)
		{
			if(pDlg->m_duScan.m_ppDirChange[i]) delete pDlg->m_duScan.m_ppDirChange[i];
			pDlg->m_duScan.m_ppDirChange[i] = NULL;
		}
		delete [] pDlg->m_duScan.m_ppDirChange;
		pDlg->m_duScan.m_ppDirChange = NULL;
	}

	EnterCriticalSection (&pDlg->m_duScan.m_critDirCanon);
	if(pDlg->m_duScan.m_ppDirCanon)
	{
		unsigned long i = 0;
		for(i=0; i<pDlg->m_duScan.m_ulNumDirEntries; i++)
		{
			if(pDlg->m_duScan.m_ppDirCanon[i]) delete pDlg->m_duScan.m_ppDirCanon[i];
			pDlg->m_duScan.m_ppDirCanon[i] = NULL;
		}
		delete [] pDlg->m_duScan.m_ppDirCanon;
		pDlg->m_duScan.m_ppDirCanon = NULL;
	}
	pDlg->m_duScan.m_ulNumDirChanges = 0;
	pDlg->m_duScan.m_ulNumDirEntries = 0;

	LeaveCriticalSection (&pDlg->m_duScan.m_critDirCanon);
	LeaveCriticalSection (&pDlg->m_duScan.m_crit);
/*
int chktm = clock();
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
fprintf(fp, "%d init on %s\r\n", chktm ,pDlg->m_duScan.m_pszWatchPath);
fflush(fp);
fclose(fp);
}
* /
	//start the process up by loading up the canon
//		pDlg->m_duScan.AddInitDirEntries(pDlg->m_duScan.m_pszWatchPath);
	
EnterCriticalSection (&pDlg->m_duScan.m_critChangeThreads);
	pDlg->m_duScan.m_ulChangeThreads = 0; // let's just be sure
LeaveCriticalSection (&pDlg->m_duScan.m_critChangeThreads);

	DirData_t* pdd = new DirData_t;
	if(pdd)
	{
		CWaitCursor cw;
		pdd->pszPath = (char*)malloc(strlen(pDlg->m_duScan.m_pszWatchPath)+1);
		if(pdd->pszPath) strcpy(pdd->pszPath, pDlg->m_duScan.m_pszWatchPath);
		pdd->pDirUtilObj = &pDlg->m_duScan;
		pdd->ulOptions = WATCH_INIT;  // indicates this is initialization
		pdd->pEvent = new CEvent(FALSE, TRUE);
		if(pdd->pEvent) pdd->pEvent->ResetEvent();
		if(_beginthread( DirChangeThread, 0, (void*)(pdd) ) != -1)
		{
			WaitForSingleObject(pdd->pEvent->m_hObject, 10000); // wait for thread to start!
		}
		else if(pDlg->m_duScan.m_bWatchFolder)// try again
		{
			Sleep(1000);
			if(_beginthread( DirChangeThread, 0, (void*)(pdd) ) != -1)
			{
				WaitForSingleObject(pdd->pEvent->m_hObject, 10000); // wait for thread to start!
			}
		}

		// stall here until changes are checked

		bool bBreak=false;

		while(!bBreak)
		{
			EnterCriticalSection (&pDlg->m_duScan.m_critChangeThreads);
			if(pDlg->m_duScan.m_ulChangeThreads<=0) bBreak=true;
			LeaveCriticalSection (&pDlg->m_duScan.m_critChangeThreads);

			if(!bBreak) Sleep(5);  // wait for threads to finish
/*
fp = fopen("dirwatch.log", "at");
if(fp)
{
fprintf(fp, "%d init thread count:%d (delta %d)\r\n", clock(), pDlg->m_duScan.m_ulChangeThreads, clock()-chktm );
fflush(fp);
fclose(fp);
}
* /
		}
	}
*/

	pDlg->m_duScan.BeginWatch();

	bool bBreak=false;

	Sleep(1000);
	int nTime = clock();

	{
		CWaitCursor cw;
		while((!bBreak)&&(pDlg->m_bRunning))
		{
			EnterCriticalSection (&pDlg->m_duScan.m_critChangeThreads);
			if(pDlg->m_duScan.m_ulChangeThreads<=0) bBreak=true;

			if(nTime+10000 < clock())
			{
				nTime = clock();
				szLC.Format("Please wait: %d entries found so far", pDlg->m_duScan.m_ulNumDirEntries);
				nCount = pDlg->m_lc.GetItemCount();
				pDlg->m_lc.InsertItem(nCount, szLC);
				pDlg->m_lc.EnsureVisible(nCount, FALSE);
			}

			LeaveCriticalSection (&pDlg->m_duScan.m_critChangeThreads);

			if(!bBreak) Sleep(5);  // wait for threads to finish
		}
	}
	pDlg->m_duScan.EndWatch();

	if(pDlg->m_bRunning)
	{
		szLC.Format("Directory %s contained %d entries", pDlg->m_szScan, pDlg->m_duScan.m_ulNumDirEntries);
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
	}
	else
	{
		szLC.Format("Directory scan cancelled");
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
		goto endofthread;
	}



	szLC.Format("Scanning comparison directory %s", pDlg->m_szComp);
	nCount = pDlg->m_lc.GetItemCount();
	pDlg->m_lc.InsertItem(nCount, szLC);
	pDlg->m_lc.EnsureVisible(nCount, FALSE);


	pDlg->m_duComp.SetWatchFolder( pDlg->m_szComp.GetBuffer(0));

	pDlg->m_duComp.BeginWatch();

	bBreak=false;

	Sleep(1000);

	nTime = clock();

	{
		CWaitCursor cw;
		while((!bBreak)&&(pDlg->m_bRunning))
		{
			EnterCriticalSection (&pDlg->m_duComp.m_critChangeThreads);
			if(pDlg->m_duComp.m_ulChangeThreads<=0) bBreak=true;

			if(nTime+10000 < clock())
			{
				nTime = clock();
				szLC.Format("Please wait: %d entries found so far", pDlg->m_duComp.m_ulNumDirEntries);
				nCount = pDlg->m_lc.GetItemCount();
				pDlg->m_lc.InsertItem(nCount, szLC);
				pDlg->m_lc.EnsureVisible(nCount, FALSE);
			}

			LeaveCriticalSection (&pDlg->m_duComp.m_critChangeThreads);

			if(!bBreak) Sleep(5);  // wait for threads to finish
		}
	}

	pDlg->m_duComp.EndWatch();



	if(pDlg->m_bRunning)
	{
		szLC.Format("Comparison directory %s contained %d entries", pDlg->m_szScan, pDlg->m_duComp.m_ulNumDirEntries);
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
	}
	else
	{
		szLC.Format("Directory scan cancelled");
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
		goto endofthread;
	}


	if(pDlg->m_bPurge)
	{


	szLC.Format("Scanning %d entries in %s for purged files", pDlg->m_duScan.m_ulNumDirEntries, pDlg->m_szScan);
	nCount = pDlg->m_lc.GetItemCount();
	pDlg->m_lc.InsertItem(nCount, szLC);
	pDlg->m_lc.EnsureVisible(nCount, FALSE);

	nTime = clock();

	while((i<pDlg->m_duScan.m_ulNumDirEntries)&&(pDlg->m_bRunning))
	{
		if(nTime+10000 < clock())
		{
			nTime = clock();
			szLC.Format("Please wait: %d entries scanned so far", i);
			nCount = pDlg->m_lc.GetItemCount();
			pDlg->m_lc.InsertItem(nCount, szLC);
			pDlg->m_lc.EnsureVisible(nCount, FALSE);
		}

		if((pDlg->m_duScan.m_ppDirCanon)&&(pDlg->m_duScan.m_ppDirCanon[i]))
		{
			/// pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename

			if(pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename)
			{
				unsigned long c=0;
				while((c<pDlg->m_duComp.m_ulNumDirEntries)&&(pDlg->m_bRunning))
				{
					if(nTime+10000 < clock())
					{
						nTime = clock();
						szLC.Format("Please wait: %d entries scanned so far", i);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
					}

					if((pDlg->m_duComp.m_ppDirCanon)&&(pDlg->m_duComp.m_ppDirCanon[c]))
					{
						if(pDlg->m_duComp.m_ppDirCanon[c]->m_szFilename)
						{
							if(stricmp(pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename, pDlg->m_duComp.m_ppDirCanon[c]->m_szFilename) == 0 )
							{
								break;
							}
						}
					}

					c++;
				}

				if((c>=pDlg->m_duComp.m_ulNumDirEntries)&&(pDlg->m_bRunning))
				{
					// not found at all, 
						szLC.Format("not found: %s (in %s)", pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename, pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);


	//make dirs if nec.
	char path_buffer[_MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char final_buffer[_MAX_PATH];

	strcpy(path_buffer, pDlg->m_szScan.GetBuffer(0));

	_splitpath( path_buffer, drive, dir, fname, ext );
	
	if((pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath)&&(strlen(pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath)))
	{
		strcpy(final_buffer, pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath+pDlg->m_szScan.GetLength());
		strcpy(dir, "Purged");
		strcat(dir, final_buffer);
	}

/*					szLC.Format("dir %s", dir);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
*/

	// have to make dir.
	// if dir already exists, no problem, _mkdir just returns and we continue
	// so, we can always just call mkdir

	if(strlen(drive)>0)
	{
		strcpy(final_buffer, drive);
		if((dir[0]!='\\')&&(dir[0]!='/'))
		{
			// need to ensure a dir delimiter between drive and directory
			// bu.MakeFileString below does the same thing.
			strcat(final_buffer, "\\");
		}
		strcat(final_buffer, dir);
	}
	else
	{
		// want to remove any leading directory delimiters in the case of no drive specified (using current directory)
		// actually, we can leave it, it goes to the root dir
		strcpy(final_buffer, dir);
	}

	// needs a final backslash
	strcat(final_buffer, "\\");


	int nLen = (int)strlen(final_buffer);
	int dr=0;
	for (int q=0; q<nLen; q++)
	{
		if((final_buffer[q]=='/')||(final_buffer[q]=='\\'))
		{
			path_buffer[q] = 0;
			if(strlen(path_buffer)>0)
			{
				if(dr!=0)
				{

/*
							szLC.Format("making %s", path_buffer);
							nCount = pDlg->m_lc.GetItemCount();
							pDlg->m_lc.InsertItem(nCount, szLC);
							pDlg->m_lc.EnsureVisible(nCount, FALSE);
*/
					_mkdir(path_buffer);
				}
				dr++;
			}
			path_buffer[q] = '\\';
		}
		else
			path_buffer[q] = final_buffer[q];
	}


	sprintf(path_buffer, "%s\\%s", pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath, pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename);

	char dest_buffer[_MAX_PATH];
	sprintf(dest_buffer, "%s%s", final_buffer, pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename); // final buffer has trailing slash

 if(	MoveFile(path_buffer, dest_buffer))
 {
						szLC.Format("moved file: %s -> %s)", path_buffer, dest_buffer);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
						p++;
 }
 else
 {
						szLC.Format("error moving file: %s -> %s)", path_buffer, dest_buffer);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
 }

				}
			}
		}

		i++;
	}

	}


	if(pDlg->m_bCopy)
	{

	szLC.Format("Scanning %d entries in %s for added files", pDlg->m_duComp.m_ulNumDirEntries, pDlg->m_szComp);
	nCount = pDlg->m_lc.GetItemCount(); 
	pDlg->m_lc.InsertItem(nCount, szLC);
	pDlg->m_lc.EnsureVisible(nCount, FALSE);

	i=0;
	nTime = clock();
	while((i<pDlg->m_duComp.m_ulNumDirEntries)&&(pDlg->m_bRunning))
	{
		if(nTime+10000 < clock())
		{
			nTime = clock();
			szLC.Format("Please wait: %d entries scanned so far", i);
			nCount = pDlg->m_lc.GetItemCount();
			pDlg->m_lc.InsertItem(nCount, szLC);
			pDlg->m_lc.EnsureVisible(nCount, FALSE);
		}

		if((pDlg->m_duComp.m_ppDirCanon)&&(pDlg->m_duComp.m_ppDirCanon[i]))
		{
			/// pDlg->m_duScan.m_ppDirCanon[i]->m_szFilename

			if(pDlg->m_duComp.m_ppDirCanon[i]->m_szFilename)
			{
				unsigned long c=0;
				while((c<pDlg->m_duScan.m_ulNumDirEntries)&&(pDlg->m_bRunning))
				{
					if(nTime+10000 < clock())
					{
						nTime = clock();
						szLC.Format("Please wait: %d entries scanned so far", i);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
					}

					if((pDlg->m_duScan.m_ppDirCanon)&&(pDlg->m_duScan.m_ppDirCanon[c]))
					{
						if(pDlg->m_duScan.m_ppDirCanon[c]->m_szFilename)
						{
							if(stricmp(pDlg->m_duComp.m_ppDirCanon[i]->m_szFilename, pDlg->m_duScan.m_ppDirCanon[c]->m_szFilename) == 0 )
							{
								break;
							}
						}
					}

					c++;
				}

				if((c>=pDlg->m_duScan.m_ulNumDirEntries)&&(pDlg->m_bRunning))
				{
					// not found at all, 
						szLC.Format("not found: %s (in %s)", pDlg->m_duComp.m_ppDirCanon[i]->m_szFilename, pDlg->m_duComp.m_ppDirCanon[i]->m_szFullPath);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);


	//make dirs if nec.
	char path_buffer[_MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char final_buffer[_MAX_PATH];

	strcpy(path_buffer, pDlg->m_szScan.GetBuffer(0));

	_splitpath( path_buffer, drive, dir, fname, ext );
	
	if((pDlg->m_duComp.m_ppDirCanon[i]->m_szFullPath)&&(strlen(pDlg->m_duComp.m_ppDirCanon[i]->m_szFullPath)))
	{
		strcpy(final_buffer, pDlg->m_duScan.m_ppDirCanon[i]->m_szFullPath+pDlg->m_szComp.GetLength());
		strcpy(dir, pDlg->m_szScan.GetBuffer(0));
		strcat(dir, final_buffer);
	}

/*
					szLC.Format("dir %s", dir);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
*/

	// have to make dir.
	// if dir already exists, no problem, _mkdir just returns and we continue
	// so, we can always just call mkdir

/*  // remove this because the comp path includes the drive
	if(strlen(drive)>0)
	{
		strcpy(final_buffer, drive);
		if((dir[0]!='\\')&&(dir[0]!='/'))
		{
			// need to ensure a dir delimiter between drive and directory
			// bu.MakeFileString below does the same thing.
			strcat(final_buffer, "\\");
		}
		strcat(final_buffer, dir);
	}
	else
*/
	
	{
		// want to remove any leading directory delimiters in the case of no drive specified (using current directory)
		// actually, we can leave it, it goes to the root dir
		strcpy(final_buffer, dir);
	}

	// needs a final backslash
	strcat(final_buffer, "\\");


	int nLen = (int)strlen(final_buffer);
	int dr=0;
	for (int q=0; q<nLen; q++)
	{
		if((final_buffer[q]=='/')||(final_buffer[q]=='\\'))
		{
			path_buffer[q] = 0;
			if(strlen(path_buffer)>0)
			{
				if(dr!=0)
				{

/*
							szLC.Format("making %s", path_buffer);
							nCount = pDlg->m_lc.GetItemCount();
							pDlg->m_lc.InsertItem(nCount, szLC);
							pDlg->m_lc.EnsureVisible(nCount, FALSE);
*/
					_mkdir(path_buffer);
				}
				dr++;
			}
			path_buffer[q] = '\\';
		}
		else
			path_buffer[q] = final_buffer[q];
	}

	if(!(pDlg->m_duComp.m_ppDirCanon[i]->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY))
	{
		sprintf(path_buffer, "%s\\%s", pDlg->m_duComp.m_ppDirCanon[i]->m_szFullPath, pDlg->m_duComp.m_ppDirCanon[i]->m_szFilename);

		char dest_buffer[_MAX_PATH];
		sprintf(dest_buffer, "%s%s", final_buffer, pDlg->m_duComp.m_ppDirCanon[i]->m_szFilename); // final buffer has trailing slash

 if(	CopyFile(path_buffer, dest_buffer, TRUE))
 {
						szLC.Format("copied file: %s -> %s)", path_buffer, dest_buffer);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
						a++;
 }
 else
 {
						szLC.Format("error copying file: %s -> %s)", path_buffer, dest_buffer);
						nCount = pDlg->m_lc.GetItemCount();
						pDlg->m_lc.InsertItem(nCount, szLC);
						pDlg->m_lc.EnsureVisible(nCount, FALSE);
 }

	}
				}
			}
		}

		i++;
	}
	}

	if(pDlg->m_bRunning)
	{
		szLC.Format("scanned %d entries, excluded %d, added %d", i,p,a);
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
	}
	else
	{
		szLC.Format("Directory scan cancelled");
		nCount = pDlg->m_lc.GetItemCount();
		pDlg->m_lc.InsertItem(nCount, szLC);
		pDlg->m_lc.EnsureVisible(nCount, FALSE);
		goto endofthread;
	}


endofthread:

	pDlg->GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Start");
	pDlg->GetDlgItem(IDC_EDIT_SCANDIR)->EnableWindow(TRUE);
	pDlg->GetDlgItem(IDC_EDIT_COMPDIR)->EnableWindow(TRUE);
//	pDlg->GetDlgItem(IDC_CHECK_COPY)->EnableWindow(TRUE);
	pDlg->GetDlgItem(IDC_CHECK_PURGE)->EnableWindow(TRUE);

	if(pDlg->m_bRunning) AfxMessageBox("Done!");
	pDlg->m_bRunning = FALSE;
}

