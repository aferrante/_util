; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CFileSortDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "FileSort.h"

ClassCount=3
Class1=CFileSortApp
Class2=CFileSortDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_FILESORT_DIALOG

[CLS:CFileSortApp]
Type=0
HeaderFile=FileSort.h
ImplementationFile=FileSort.cpp
Filter=N

[CLS:CFileSortDlg]
Type=0
HeaderFile=FileSortDlg.h
ImplementationFile=FileSortDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_LIST1

[CLS:CAboutDlg]
Type=0
HeaderFile=FileSortDlg.h
ImplementationFile=FileSortDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_FILESORT_DIALOG]
Type=1
Class=CFileSortDlg
ControlCount=10
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_SCANDIR,edit,1350631552
Control4=IDC_STATIC,static,1342308354
Control5=IDC_EDIT_COMPDIR,edit,1350631552
Control6=IDC_STATIC,static,1342308354
Control7=IDC_BUTTON_GO,button,1342242816
Control8=IDC_LIST1,SysListView32,1350680585
Control9=IDC_CHECK_PURGE,button,1342242819
Control10=IDC_CHECK_COPY,button,1476460547

