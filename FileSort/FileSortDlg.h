// FileSortDlg.h : header file
//

#if !defined(AFX_FILESORTDLG_H__D5DDD400_C2DB_40F3_8901_7CCD161B9CF8__INCLUDED_)
#define AFX_FILESORTDLG_H__D5DDD400_C2DB_40F3_8901_7CCD161B9CF8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "..\..\Common\FILE\DirUtil.h"



/////////////////////////////////////////////////////////////////////////////
// CFileSortDlg dialog

class CFileSortDlg : public CDialog
{
// Construction
public:
	CFileSortDlg(CWnd* pParent = NULL);	// standard constructor


	BOOL m_bRunning;

	CDirUtil m_duScan;
	CDirUtil m_duComp;


// Dialog Data
	//{{AFX_DATA(CFileSortDlg)
	enum { IDD = IDD_FILESORT_DIALOG };
	CListCtrl	m_lc;
	CString	m_szComp;
	CString	m_szScan;
	BOOL	m_bPurge;
	BOOL	m_bCopy;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileSortDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CFileSortDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonGo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILESORTDLG_H__D5DDD400_C2DB_40F3_8901_7CCD161B9CF8__INCLUDED_)
