// ServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Server.h"
#include "ServerDlg.h"
#include "..\..\Common\TXT\BufferUtil.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void TestServerHandlerThread(void* pvArgs);
CWnd* g_pwnd;
CWnd* g_pwnd2;
BOOL g_bTermZero = FALSE;
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerDlg dialog

CServerDlg::CServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CServerDlg)
	m_nPort = 10999;
	m_szRecv = _T("");
	m_szResponse = _T("<xml>\r\n</xml>");
	m_bPersist = FALSE;
	m_bTermZero = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_bStarted = FALSE;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServerDlg)
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDX_Text(pDX, IDC_EDIT_RECV, m_szRecv);
	DDX_Text(pDX, IDC_EDIT_RESPONSE, m_szResponse);
	DDX_Check(pDX, IDC_CHECK_PERSIST, m_bPersist);
	DDX_Check(pDX, IDC_CHECK_TERMZERO, m_bTermZero);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CServerDlg, CDialog)
	//{{AFX_MSG_MAP(CServerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerDlg message handlers

BOOL CServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
			g_pwnd = GetDlgItem(IDC_EDIT_RECV);
			g_pwnd2 = GetDlgItem(IDC_EDIT_RESPONSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CServerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CServerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CServerDlg::OnButtonStart() 
{
	if(m_bStarted)
	{
		char buffer[NET_ERRORSTRING_LEN];
		if(m_net.StopServer(m_nPort, 5000, buffer)<NET_SUCCESS)
		{
			AfxMessageBox(buffer);
		}
		else
		{
			m_bStarted = FALSE;
		}
	}
	else
	{
		UpdateData(TRUE);
		if(m_nPort>0)
		{
			CNetServer* pServer = new CNetServer;
			char buffer[NET_ERRORSTRING_LEN];
			pServer->m_usPort = m_nPort;
			pServer->m_lpfnHandler = TestServerHandlerThread;
			if(m_bPersist)
				pServer->m_ucType |= NET_TYPE_KEEPOPEN;

			if(m_net.StartServer(pServer, NULL, 10000, buffer)<NET_SUCCESS)
			{
				AfxMessageBox(buffer);
			}
			else
			{
				m_bStarted = TRUE;
				g_bTermZero = m_bTermZero;
			}
		}
	}

	if(m_bStarted)
		GetDlgItem(IDC_BUTTON_START)->SetWindowText("Stop");
	else
		GetDlgItem(IDC_BUTTON_START)->SetWindowText("Start");
}

void CServerDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CServerDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void TestServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;


		bool bCloseCommand = false;

		do
		{
			// get the raw buffer and process it yourself
			unsigned char* pch; 
			unsigned long ulBufLen;

// AfxMessageBox("now");

			nReturn = net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_ONCE, pszStatus);

			char buffer9[125]; sprintf(buffer9,"%d",ulBufLen);
// AfxMessageBox(buffer9);

 			data.m_ulDataLen = ulBufLen;
			CBufferUtil bu; 
			
			char* pch2 = bu.ReadableHex((char*)pch, &ulBufLen, 0);
// AfxMessageBox(pch2);

			if(g_pwnd) g_pwnd->SetWindowText(pch2);

			free(pch2);
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);
			nReturn = net.InterpretData(pch, &data, true, pszStatus);
// AfxMessageBox((char*)pch);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
	sprintf(buffer9,"%d",nReturn);
// AfxMessageBox(buffer9);
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(1)//(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					//nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply


//					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply

					CString szBuffer;
					if(g_pwnd2) g_pwnd2->GetWindowText(szBuffer);

					unsigned long ulBufferLen = szBuffer.GetLength();
					if(g_bTermZero) ulBufferLen++;
					char* pchBuffer = (char*) malloc(ulBufferLen+1);
					sprintf(pchBuffer, "%s", szBuffer);
										
					nReturn = net.SendLine((unsigned char*) pchBuffer, ulBufferLen, pClient->m_socket, EOLN_NONE, true, 10000, pszStatus);

					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.


		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
}


