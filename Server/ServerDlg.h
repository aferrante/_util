// ServerDlg.h : header file
//

#if !defined(AFX_SERVERDLG_H__A5524C8F_DFAB_4F43_9E32_D8D4B0BE33AE__INCLUDED_)
#define AFX_SERVERDLG_H__A5524C8F_DFAB_4F43_9E32_D8D4B0BE33AE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CServerDlg dialog
#include "..\..\Common\LAN\NetUtil.h"

class CServerDlg : public CDialog
{
// Construction
public:
	CServerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CServerDlg)
	enum { IDD = IDD_SERVER_DIALOG };
	int		m_nPort;
	CString	m_szRecv;
	CString	m_szResponse;
	BOOL	m_bPersist;
	BOOL	m_bTermZero;
	//}}AFX_DATA

	BOOL m_bStarted;
	CNetUtil m_net;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CServerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonStart();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERDLG_H__A5524C8F_DFAB_4F43_9E32_D8D4B0BE33AE__INCLUDED_)
