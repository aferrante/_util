// DropFrameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DropFrame.h"
#include "DropFrameDlg.h"
#include <sys\timeb.h>
#include <time.h>
#include "..\..\Common\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CDropFrameApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
/*
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

*/


/////////////////////////////////////////////////////////////////////////////
// CDropFrameDlg dialog

CDropFrameDlg::CDropFrameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDropFrameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDropFrameDlg)
	m_bNTSC = 0;
	m_szNTSC = _T("");
	m_szMilliseconds = _T("");
	m_szOutput = _T("");
	m_nFPS = 25; //PAL
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nSource =0;
}

void CDropFrameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDropFrameDlg)
	DDX_Radio(pDX, IDC_RADIO_MS, m_bNTSC);
	DDX_Text(pDX, IDC_EDIT_UTC, m_szNTSC);
	DDX_Text(pDX, IDC_EDIT_UNIX, m_szMilliseconds);
	DDX_Text(pDX, IDC_EDIT_LOCAL, m_szOutput);
	DDX_Text(pDX, IDC_EDIT_FPS, m_nFPS);
	DDV_MinMaxUInt(pDX, m_nFPS, 1, 1000000);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDropFrameDlg, CDialog)
	//{{AFX_MSG_MAP(CDropFrameDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CHECK_PUSHPIN, OnCheckPushpin)
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	ON_BN_CLICKED(IDC_BUTTON_NOW, OnButtonNow)
	ON_EN_SETFOCUS(IDC_EDIT_LOCAL, OnSetfocusEditLocal)
	ON_EN_SETFOCUS(IDC_EDIT_UNIX, OnSetfocusEditUnix)
	ON_EN_SETFOCUS(IDC_EDIT_UTC, OnSetfocusEditUtc)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_RADIO_FRAMES, OnRadioFrames)
	ON_BN_CLICKED(IDC_RADIO_MS, OnRadioMs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDropFrameDlg message handlers

BOOL CDropFrameDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.
/*
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
*/

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	OnButtonNow();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDropFrameDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
/*
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
*/
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDropFrameDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDropFrameDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDropFrameDlg::OnCheckPushpin() 
{
	// TODO: Add your control notification handler code here
	if(((CButton*)GetDlgItem(IDC_CHECK_PUSHPIN))->GetCheck())
		SetWindowPos(	&wndTopMost, 0,0,0,0, SWP_NOMOVE|SWP_NOSIZE);
	else
		SetWindowPos(	&wndNoTopMost, 0,0,0,0, SWP_NOMOVE|SWP_NOSIZE);
}

void CDropFrameDlg::OnButtonGo() 
{
	// TODO: Add your control notification handler code here
	if(!UpdateData(TRUE)) return;

	int nMilliseconds = 0;
	unsigned long ulHours; 
	unsigned char ucMinutes; 
	unsigned char ucSeconds; 
	unsigned long ulFrames; 

	switch(m_nSource)
	{
	case 1: //df or fps input
		{
//			AfxMessageBox("df");
			CSafeBufferUtil sbu;

			unsigned long ulBufferLength = m_szOutput.GetLength();

			char* pch = sbu.Token(m_szOutput.GetBuffer(0), ulBufferLength, " :.;");

			if(pch)
				ulHours = (unsigned long)(atol(pch));
			else
				ulHours = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ucMinutes = (unsigned char)(atol(pch));
			else
				ucMinutes = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ucSeconds = (unsigned char)(atol(pch));
			else
				ucSeconds = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ulFrames = (unsigned long)(atol(pch));
			else
				ulFrames = 0;

			if(m_bNTSC==0)
				nMilliseconds = ConvertHMSFToMilliseconds(ulHours, ucMinutes, ucSeconds, ulFrames, (float)29.9200);
			else
				nMilliseconds = ConvertHMSFToMilliseconds(ulHours, ucMinutes, ucSeconds, ulFrames, (float)m_nFPS);

			if(nMilliseconds == TIME_NOT_DEFINED)
				m_szMilliseconds.Format("error");
			else
				m_szMilliseconds.Format("%d", nMilliseconds);

		} break;
	case 2: //utc input
		{
	//		AfxMessageBox("utc");
			CSafeBufferUtil sbu;

			unsigned long ulBufferLength = m_szNTSC.GetLength();

			char* pch = sbu.Token(m_szNTSC.GetBuffer(0), ulBufferLength, " :.;");

			if(pch)
				ulHours = (unsigned long)(atol(pch));
			else
				ulHours = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ucMinutes = (unsigned char)(atol(pch));
			else
				ucMinutes = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ucSeconds = (unsigned char)(atol(pch));
			else
				ucSeconds = 0;

			pch = sbu.Token(NULL, NULL, " :.;");

			if(pch)
				ulFrames = (unsigned long)(atol(pch));
			else
				ulFrames = 0;

			nMilliseconds = ConvertHMSFToMilliseconds(ulHours, ucMinutes, ucSeconds, ulFrames, 30.0);
			if(nMilliseconds == TIME_NOT_DEFINED)
				m_szMilliseconds.Format("error");
			else
				m_szMilliseconds.Format("%d", nMilliseconds);
		} break;
	default:
	case 0: //unixtime input
		{
		} break;
	}

	if(nMilliseconds == TIME_NOT_DEFINED)
	{
		// nothing, leave it so you can review error.
	}
	else
	{
	
			AfxMessageBox(m_szMilliseconds);

		nMilliseconds = atoi(m_szMilliseconds);

		// convert ms to df or pal etc.
		if(m_bNTSC==0)
		{
			// do drop frame

			ConvertMillisecondsToHMSF(((unsigned long) nMilliseconds), 
																 &ulHours, 
																 &ucMinutes, 
																 &ucSeconds, 
																 &ulFrames, 
																 (float)29.92);

			m_szOutput.Format("%02d:%02d:%02d;%02d",
				ulHours, ucMinutes, ucSeconds, ulFrames);

		}
		else
		{
			ConvertMillisecondsToHMSF(((unsigned long) nMilliseconds), 
																 &ulHours, 
																 &ucMinutes, 
																 &ucSeconds, 
																 &ulFrames, 
																 (float)m_nFPS);

			int nWidth = 0;
			int nFrames = m_nFPS;
			while(nFrames>0)
			{
				nWidth++;
				nFrames /= 10;
			}

			if(nWidth<0) nWidth=0;
			if(nWidth>10) nWidth=10;
		
			char frmbuf[128];
			sprintf(frmbuf, "%%02d:%%02d:%%02d.%%0%dd", nWidth);
			m_szOutput.Format(frmbuf, ulHours, ucMinutes, ucSeconds, ulFrames);
		}


		// do regular ntsc

		ConvertMillisecondsToHMSF(((unsigned long) nMilliseconds), 
															 &ulHours, 
															 &ucMinutes, 
															 &ucSeconds, 
															 &ulFrames, 
															 30.0);

		m_szNTSC.Format("%02d:%02d:%02d.%02d",
			ulHours, ucMinutes, ucSeconds, ulFrames);
	}

	UpdateData(FALSE);
}

void CDropFrameDlg::OnButtonNow() 
{
	_timeb now;
	_ftime( &now );

	m_szMilliseconds.Format("%d%03d", now.time%86400 - (now.timezone*60) + (now.dstflag?60:0), now.millitm); // the mod makes it todays number of milliseconds
//	GetDlgItem(IDC_EDIT_UNIX)->SetWindowText(m_szMilliseconds);
	UpdateData(FALSE);

	m_nSource = 0;

	OnButtonGo();
}

BOOL CDropFrameDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CDropFrameDlg::OnSetfocusEditLocal() 
{
	m_nSource = 1;
}

void CDropFrameDlg::OnSetfocusEditUnix() 
{
	m_nSource = 0;
}

void CDropFrameDlg::OnSetfocusEditUtc() 
{
	m_nSource = 2;
}

void CDropFrameDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	int l = theApp.GetProfileInt(_T("Position"), _T("Left"),   -1);  
	int t = theApp.GetProfileInt(_T("Position"), _T("Top"),    -1);  
	theApp.WriteProfileInt(_T("Position"), _T("Left"),   l);      
	theApp.WriteProfileInt(_T("Position"), _T("Top"),    t);      
	if((l>=0)&&(t>=0))
	{
		//check off screen
		if (l>=GetSystemMetrics(SM_CXSCREEN)) { l=0;}
		if (t>=GetSystemMetrics(SM_CYSCREEN)) { t=0;}
		SetWindowPos(	&wndTop, l,t,0,0, SWP_NOZORDER|SWP_NOSIZE);
	}
	
}

void CDropFrameDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	WINDOWPLACEMENT wpl;
	GetWindowPlacement(&wpl);

	theApp.WriteProfileInt(_T("Position"), _T("Left"),   wpl.rcNormalPosition.left);      
	theApp.WriteProfileInt(_T("Position"), _T("Top"),    wpl.rcNormalPosition.top);      
	
	CDialog::OnCancel();
}

void CDropFrameDlg::OnOK() 
{
	OnButtonGo();	
	
//	CDialog::OnOK();
}

void CDropFrameDlg::OnRadioFrames() 
{
	OnButtonGo();	
}

void CDropFrameDlg::OnRadioMs() 
{
	OnButtonGo();	
}

unsigned long CDropFrameDlg::ConvertHMSFToMilliseconds(unsigned long ulHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned long ulFrames, float flFrameBasis) 
{
CString foo;
	if/*((ulHours>23)||*/(ulHours<0)/*)*/ return TIME_NOT_DEFINED;  // allow larger than one day
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;
	
	if((flFrameBasis <= 29.920001)&&(flFrameBasis >= 29.919999)) //NTSC DF
	//NTSC DF
	{
		if((ulFrames>29)||(ulFrames<0)) return TIME_NOT_DEFINED;
	AfxMessageBox(" ConvertHMSFToMilliseconds 29.92");


    unsigned long ulNunFrames = 
        (ulHours * 108000) + 
        (ucMinutes * 1800) + 
        (ucSeconds * 30) + 
        ulFrames;

    unsigned long ulNumMins = (ulHours * 60) + ucMinutes;

    unsigned long ulDropped = ( ulNumMins - ( ulNumMins / 10 ) ) * 2;

    return (unsigned long)( ( ( (double)( ulNunFrames - ulDropped ) * 1001.0 ) / 30.0 ) + 0.5 ); 

	}
	else  // other frame rate. must be integer.
	{
foo.Format("ConvertHMSFToMilliseconds %d frame rate, %d frames / %d", (int)flFrameBasis , ulFrames, ((int)(flFrameBasis)) );
	AfxMessageBox(foo);
		if((ulFrames>((unsigned long)(flFrameBasis)-1))||(ulFrames<0)) return TIME_NOT_DEFINED;
		return (ulHours*3600000+ucMinutes*60000+ucSeconds*1000+(ulFrames*1000)/((int)(flFrameBasis))); //parens for rounding error
	}
}

void  CDropFrameDlg::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned long* pulHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned long* pulFrames, float flFrameBasis) 
{	
	CString foo; foo.Format("%d",ulMilliseconds );
//	AfxMessageBox(foo);

	if((flFrameBasis <= 29.920001)&&(flFrameBasis >= 29.919999)) //NTSC DF
	{
	AfxMessageBox(" ConvertMillisecondsToHMSF 29.92");
		unsigned long frames = (unsigned long)( ((((double)(ulMilliseconds)*30.0) / 1001.0) + 0.5));  //round up.
    unsigned long tenmins = (unsigned long)(frames / 17982);
    unsigned long rem = frames % 17982;
    if (rem >= 1800) 
    { 
			rem += (2 + ((rem - 1800) / 1798) * 2); 
    }
    *pulHours	= ((unsigned long)(tenmins/6)); 
    *pucMinutes = ((unsigned char)(((tenmins % 6) * 10) + (rem / 1800))); 
    *pucSeconds = ((unsigned char)((rem % 1800) / 30)); 
    *pulFrames	= ((unsigned long)(rem % 30));
	}
	else // other frame rate. must be integer.
	{
foo.Format("ConvertMillisecondsToHMSF %d frame rate, %d ms / %f", (int)flFrameBasis , ulMilliseconds%1000L, (1000.0/flFrameBasis) );
	AfxMessageBox(foo);
		*pulHours		= ((unsigned long)(ulMilliseconds/3600000L));
		*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
		*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;
		*pulFrames = ((unsigned long)( ( ((float)(ulMilliseconds%1000L))/(1000.0/flFrameBasis)) ) );  //(int)(1000.0/25.0) = 40
	}
}

/*

                // Drop-Frame Calculation 
                // 
                // Drop frame is a method of representing a count of real 
                // NTSC frames (30/1.001 frames per second) in an HH:MM:SS.FF 
                // format.  It counts the real frames, but counts them as if 
                // there were exactly 30 frames per second.  In order to keep 
                // the readout closer to the actual hours:minutes:seconds 
                // time, the readout skips or "drops" two frames (frame 00 
                // and frame 01) every minute, except every tenth minute. 
                // 
                // See below: 
                // 
                // Frame  Actual time (.ms)  Drop-frame 
                //     0  00:00:00.000       00:00:00.00 
                //     1  00:00:00.033       00:00:00.01 
                //     2  00:00:00.067       00:00:00.02 
                //     3  00:00:00.100       00:00:00.03 
                //   ...           ...               ... 
                //    29  00:00:00.968       00:00:00.29  <- counting as if 
                //    30  00:00:01.001       00:00:01.00     exactly 30 fps 
                //   ...           ...               ... 
                //    59  00:00:01.967       00:00:01.29 
                //    60  00:00:02.002       00:00:02.00 
                //   ...           ...               ... 
                //  1799  00:01:00.027       00:00:59.29 
                //  1800  00:01:00.060       00:01:00.02  <- skipped 00, 01 
                //  1801  00:01:00.093       00:01:00.03 
                //   ...           ...               ... 
                //  3597  00:02:00.020       00:01:59.29 
                //  3598  00:02:00.053       00:02:00.02  <- skipped 00, 01 
                //  3599  00:02:00.087       00:02:00.03 
                //   ...           ...               ... 
                // 17981  00:09:59.966       00:09:59.29 
                // 17982  00:09:59.999       00:10:00.00  <- not  
                // 17983  00:10:00.033       00:10:00.01  <- skipped 
                // 17984  00:10:00.066       00:10:00.02 
                // 17985  00:10:00.100       00:10:00.03 
                //   ...           ...               ... 
                // 19781  00:11:00.026       00:10:59.29 
                // 19782  00:11:00.059       00:11:00.02  <- skipped 00, 01 
                // 19783  00:11:00.093       00:11:00.03 
                // 
                // The frame count, and therefore the drop frame 
                // calculation, is reset completely every 24 hours (one day).

                // 1) Calculate the number of real NTSC frames, at a rate 
                //    of 30/1.001 frames per second.  The use of +0.5 to 
                //    achieve rounding is acceptable here as we need an 
                //    integral frame count and do not care about partial 
                //    frames. 
                long realframes = (long)(Convert.ToInt64(((milliseconds * 30.0) / 1001.0) + 0.5));

                // 2) Count the number of 17982-frame intervals, since this 
                //    easily translates into 10-minute intervals on the 
                //    HH:MM:SS.FF readout. 
                long tenminutes = (long)(realframes / 17982);

                // 3) Count the number of frames past the 17982-frame 
                //    interval boundary. 
                long remainder = realframes % 17982;

                // 4) Check if the remainder is greater than a minute. 
                //    If it is greater than a minute, we'll have to skip, 
                //    i.e. "drop", some frames. 
                if (remainder >= 1800) 
                { 
                    // 5) Calculate and add in the number of skipped, i.e. 
                    //    "dropped", frames.  After the first 1800 frames, 
                    //    this will occur every 1798 frames. 
                    remainder += 2 + ((remainder - 1800) / 1798) * 2; 
                }

                // 6) Break it all down, using the number of ten-minute 
                //    intervals calculated first, and then adding in the 
                //    number of following frames.  Now that the skipped, 
                //    i.e. "dropped", frames have been added in, we can 
                //    calculate as if there were exactly 30 frames per 
                //    second. 
                long hours = tenminutes / 6; 
                long minutes = ((tenminutes % 6) * 10) + (remainder / 1800); 
                long seconds = (remainder % 1800) / 30; 
                long frames = remainder % 30;

                // 7) Format the result as a string. 
                result = String.Format( "{0:D2}:{1:D2}:{2:D2}.{3:D2}", 
                                        (long)hours, 
                                        (long)minutes, 
                                        (long)seconds, 
                                        (long)frames );

*/
///////////////////////////////
/*
int ConvertEncodedDurationToMilliseconds(int nEncodedDuration) 
{ 
        int nDuration = 0; 
        CStringA strHex; 
        strHex.Format("%.8X", nEncodedDuration);

        CStringA strHours = strHex.Mid(0, 2); 
        CStringA strMinutes = strHex.Mid(2, 2); 
        CStringA strSeconds = strHex.Mid(4, 2); 
        CStringA strFrames = strHex.Mid(6, 2);

        int nHours = atoi(strHours); 
        int nMinutes = atoi(strMinutes); 
        int nSeconds = atoi(strSeconds); 
        int nFrames = atoi(strFrames);

    if (m_bPal) 
    { 
            nDuration = 
                    nHours * 60 * 60 * 1000 + 
                    nMinutes * 60 * 1000 + 
                    nSeconds * 1000 + 
            nFrames * 40; 
    } 
    else 
    { 
        int totalframes = 
            nHours * 60 * 60 * 30 + 
            nMinutes * 60 * 30 + 
            nSeconds * 30 + 
            nFrames;

        int totalminutes = nHours * 60 + nMinutes;

        int droppedframes = ( totalminutes - ( totalminutes / 10 ) ) * 2;

        // Add 0.5 to accomplish (imperfect) rounding

        nDuration = (int)( ( ( ( totalframes - droppedframes ) * 1001 ) / 30.0 ) + 0.5 ); 
    }

        LogDebug(boost::format("Converted encoded duration to milliseconds: %d") %nDuration);

        return nDuration; 
}

*/

