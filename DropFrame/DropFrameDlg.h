// DropFrameDlg.h : header file
//

#if !defined(AFX_DROPFRAMEDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_)
#define AFX_DROPFRAMEDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define TIME_NOT_DEFINED			0xffffffff


/////////////////////////////////////////////////////////////////////////////
// CDropFrameDlg dialog

class CDropFrameDlg : public CDialog
{
// Construction
public:
	CDropFrameDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDropFrameDlg)
	enum { IDD = IDD_DROPFRAME_DIALOG };
	int		m_bNTSC;
	CString	m_szNTSC;
	CString	m_szMilliseconds;
	CString	m_szOutput;
	UINT	m_nFPS;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDropFrameDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

	int m_nSource;
	unsigned long ConvertHMSFToMilliseconds(unsigned long ulHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned long ulFrames, float flFrameBasis);
	void  ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned long* pulHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned long* pulFrames, float flFrameBasis);

protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDropFrameDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCheckPushpin();
	afx_msg void OnButtonGo();
	afx_msg void OnButtonNow();
	afx_msg void OnSetfocusEditLocal();
	afx_msg void OnSetfocusEditUnix();
	afx_msg void OnSetfocusEditUtc();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnRadioFrames();
	afx_msg void OnRadioMs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DROPFRAMEDLG_H__00C4F5C2_3CEE_4100_9BEA_D921A7D91AE5__INCLUDED_)
