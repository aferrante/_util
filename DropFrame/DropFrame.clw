; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDropFrameDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DropFrame.h"

ClassCount=3
Class1=CDropFrameApp
Class2=CDropFrameDlg
Class3=CAboutDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Resource2=IDD_DROPFRAME_DIALOG

[CLS:CDropFrameApp]
Type=0
HeaderFile=DropFrame.h
ImplementationFile=DropFrame.cpp
Filter=N

[CLS:CDropFrameDlg]
Type=0
HeaderFile=DropFrameDlg.h
ImplementationFile=DropFrameDlg.cpp
Filter=D
LastObject=IDC_RADIO_MS
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=DropFrameDlg.h
ImplementationFile=DropFrameDlg.cpp
Filter=D

[DLG:IDD_DROPFRAME_DIALOG]
Type=1
Class=CDropFrameDlg
ControlCount=15
Control1=IDC_BUTTON_GO,button,1342245633
Control2=IDC_BUTTON_NOW,button,1342246656
Control3=IDC_EDIT_UNIX,edit,1350631552
Control4=IDC_EDIT_UTC,edit,1350631552
Control5=IDC_EDIT_LOCAL,edit,1350631552
Control6=IDC_RADIO_MS,button,1342308361
Control7=IDC_RADIO_FRAMES,button,1342177289
Control8=IDC_EDIT_FPS,edit,1350770816
Control9=IDC_CHECK_PUSHPIN,button,1342246915
Control10=IDOK,button,1073741825
Control11=IDCANCEL,button,1073741824
Control12=IDC_STATIC,static,1342308354
Control13=IDC_STATIC,static,1342308354
Control14=IDC_STATIC,static,1342308354
Control15=IDC_STATIC,static,1342308352

