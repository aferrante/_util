; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDLLTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DLLTest.h"

ClassCount=3
Class1=CDLLTestApp
Class2=CDLLTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_DLLTEST_DIALOG

[CLS:CDLLTestApp]
Type=0
HeaderFile=DLLTest.h
ImplementationFile=DLLTest.cpp
Filter=N

[CLS:CDLLTestDlg]
Type=0
HeaderFile=DLLTestDlg.h
ImplementationFile=DLLTestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BUTTON_SS

[CLS:CAboutDlg]
Type=0
HeaderFile=DLLTestDlg.h
ImplementationFile=DLLTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DLLTEST_DIALOG]
Type=1
Class=CDLLTestDlg
ControlCount=29
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_DLL,edit,1350631552
Control5=IDC_BUTTON_LOADDLL,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_EDIT_FUNCTION1,edit,1350631552
Control8=IDC_BUTTON_GET1,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_FUNCTION2,edit,1350631552
Control11=IDC_BUTTON_GET2,button,1342242816
Control12=IDC_STATIC,static,1342308352
Control13=IDC_EDIT_FUNCTION3,edit,1350631552
Control14=IDC_BUTTON_GET3,button,1342242816
Control15=IDC_STATIC,static,1342177296
Control16=IDC_STATIC_TIME,static,1342312448
Control17=IDC_BUTTON_CB1,button,1342242816
Control18=IDC_BUTTON_CB2,button,1342242816
Control19=IDC_BUTTON_CB3,button,1342242816
Control20=IDC_BUTTON_CB4,button,1342242816
Control21=IDC_BUTTON_CS,button,1342242816
Control22=IDC_EDIT_CSTYPE,edit,1350631552
Control23=IDC_EDIT_CSDATA,edit,1350631552
Control24=IDC_BUTTON_SS,button,1342242816
Control25=IDC_EDIT_SSTYPE,edit,1350631552
Control26=IDC_EDIT_SSDATA,edit,1350631552
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352

