// TestDLL.h : main header file for the TESTDLL DLL
//

#if !defined(AFX_TESTDLL_H__56AF96DB_A1B3_4615_A91A_9FAA62FA8E78__INCLUDED_)
#define AFX_TESTDLL_H__56AF96DB_A1B3_4615_A91A_9FAA62FA8E78__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

typedef int (__stdcall* LPFNCB_SS)(char*, unsigned long); // prototype for SetStatus
typedef int (__stdcall* LPFNCB_SM)(int, char*, char*); // prototype for SendMessage
typedef int (__stdcall* LPFNCB_SLM)(unsigned long, char*, char*); // prototype for SendLogMessage


/////////////////////////////////////////////////////////////////////////////
// CTestDLLApp
// See TestDLL.cpp for the implementation of this class
//

class CTestDLLApp : public CWinApp
{
public:
	CTestDLLApp();
	int CommandState(int nType, char* pszData=NULL);
	int SetPointer(int nType, void* pFunction);
	int SetSetting(int nType, char* pValue);

	int Message(CString szText);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestDLLApp)
	public:
	virtual int ExitInstance();
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CTestDLLApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDLL_H__56AF96DB_A1B3_4615_A91A_9FAA62FA8E78__INCLUDED_)
