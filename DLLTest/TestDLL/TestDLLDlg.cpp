// TestDLLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TestDLL.h"
#include "TestDLLDlg.h"
#include "sys/timeb.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CTestDLLApp theApp;

extern _timeb* g_ptimeb;
extern LPFNCB_SS  g_pfSS;
extern LPFNCB_SM  g_pfSM;
extern LPFNCB_SLM  g_pfSLM;

/////////////////////////////////////////////////////////////////////////////
// CTestDLLDlg dialog


CTestDLLDlg::CTestDLLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDLLDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestDLLDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CTestDLLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestDLLDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestDLLDlg, CDialog)
	//{{AFX_MSG_MAP(CTestDLLDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDLLDlg message handlers

void CTestDLLDlg::OnButton1() 
{
	LPFNCB_SS lpfnCB = g_pfSS;
	
	if(lpfnCB)
	{
		srand(time(NULL));
		int ulStatus = (rand()%4);
		int nReturn = 0;

		try
		{
			switch(ulStatus)
			{
			case	0:  // YELLOW
				nReturn = (*lpfnCB)("Setting a yellow status", 0x00000010); break;
			case	1:  // RED
				nReturn = (*lpfnCB)("Setting a red status", 0x00000020); break;
			case	2:  // GREEN
				nReturn = (*lpfnCB)("Setting a green status", 0x00000030); break;
			case	3:  // BLUE
				nReturn = (*lpfnCB)("Setting a blue status", 0x00000040); break;
			}

			CString szText;
			szText.Format("SetStatus callback returned %d", nReturn);
			theApp.Message( szText );	

		}
		catch(...)
		{
			theApp.Message("An exception has occurred");	
		}

		lpfnCB = NULL;
	}
	else
	{
		theApp.Message("The function pointer for SetStatus is NULL");	
	}

}

void CTestDLLDlg::OnButton2() 
{
	LPFNCB_SM lpfnCB = g_pfSM;
	
	if(lpfnCB)
	{
		srand( time(NULL) );
		int nType = (rand()%2);

		try
		{
			switch(nType)
			{
			case	0:  // error
				nType = (*lpfnCB)(0, "TestDLL", "Sending a test error."); break;
			case	1:  // info
				nType = (*lpfnCB)(1, "TestDLL", "Sending test information."); break;
			}
			CString szText;
			szText.Format("SendMessage callback returned %d", nType);
			theApp.Message( szText );	

		}
		catch(...)
		{
			theApp.Message("An exception has occurred");	
		}
		lpfnCB = NULL;
	}
	else
	{
		theApp.Message("The function pointer for SendMessage is NULL");	
	}
}

void CTestDLLDlg::OnButton3() 
{
	LPFNCB_SLM lpfnCB = g_pfSLM;
	
	if(lpfnCB)
	{
		srand(time(NULL));
		int nType = (rand()%2);

		try
		{
			switch(nType)
			{
			case	0:  // error
				nType = (*lpfnCB)(0x00000003, "TestDLL", "Sending a test error to be logged."); break;
			case	1:  // info
				nType = (*lpfnCB)(0x00000006, "TestDLL", "Sending test information to be logged."); break;
			}

			CString szText;
			szText.Format("SendLogMessage callback returned %d", nType);
			theApp.Message( szText );	
			
		}
		catch(...)
		{
			theApp.Message("An exception has occurred");	
		}
		lpfnCB = NULL;
	}
	else
	{
		theApp.Message("The function pointer for SendLogMessage is NULL");	
	}
}

void CTestDLLDlg::OnButton4() 
{
	if(g_ptimeb)
	{
		try
		{
			_ftime(g_ptimeb);
		}
		catch(...)
		{
			theApp.Message("An exception has occurred");	
		}
	}
	else
	{
		theApp.Message("The pointer for the time object is NULL");	
	}
	
}

void CTestDLLDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
//	CDialog::OnCancel();
}

void CTestDLLDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

BOOL CTestDLLDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}


