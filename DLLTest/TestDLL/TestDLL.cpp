// TestDLL.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "TestDLL.h"
#include "TestDLLDlg.h"
#include "sys/timeb.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

CTestDLLDlg*  g_pdlg=NULL;
_timeb* g_ptimeb=NULL;
LPFNCB_SS  g_pfSS=NULL;
LPFNCB_SM  g_pfSM=NULL;
LPFNCB_SLM  g_pfSLM=NULL;
/////////////////////////////////////////////////////////////////////////////
// CTestDLLApp

BEGIN_MESSAGE_MAP(CTestDLLApp, CWinApp)
	//{{AFX_MSG_MAP(CTestDLLApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDLLApp construction

CTestDLLApp::CTestDLLApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance

}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTestDLLApp object

CTestDLLApp theApp;


int CTestDLLApp::CommandState(int nType, char* pszData)
{
	CString szText;
	szText.Format("CommandState was called with type = 0x%08x and data = %s",
		nType, 
		pszData?(strlen(pszData)?pszData:"(no data)"):"(null)"
		);
	Message( szText );
	return 0;
}

int CTestDLLApp::SetPointer(int nType, void* pFunction)
{
	CString szText;
	szText.Format("SetPointer was called with type = 0x%08x and address = 0x%08x",
		nType, 
		pFunction
		);

	switch(nType)
	{
	case 0x00000001: g_pfSS = (LPFNCB_SS) pFunction; /* AfxMessageBox("1");*/ break;
	case 0x00000002: g_pfSM = (LPFNCB_SM) pFunction;   /*AfxMessageBox("2");*/ break;
	case 0x00000003: g_pfSLM = (LPFNCB_SLM) pFunction;  /* AfxMessageBox("3");*/ break;
	case 0x00000004: g_ptimeb = (_timeb*) pFunction;   /*AfxMessageBox("4");*/ break;
	}


	Message( szText );
	return 0;
}

int CTestDLLApp::SetSetting(int nType, char* pValue)
{
	CString szText;
	szText.Format("SetSetting was called with type = 0x%08x and value = %s",
		nType, 
		pValue?(strlen(pValue)?pValue:"(no value)"):"(null)"
		);
	Message( szText );
	return 0;
}

int CTestDLLApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	if(g_pdlg) g_pdlg->DestroyWindow();
	
	return CWinApp::ExitInstance();
}

BOOL CTestDLLApp::InitInstance() 
{
	g_pdlg = new CTestDLLDlg;
	if(g_pdlg) 
	{
		if(g_pdlg->Create())
		{
			g_pdlg->ShowWindow(SW_SHOW);
			g_pdlg->SetWindowPos(NULL, 0,0,0,0, SWP_NOSIZE|SWP_NOZORDER);

		}
		else
		{
			AfxMessageBox("Could not create DLL dialog");
		}
	}
	else
	{
		AfxMessageBox("Could not create DLL dialog object");
	}
	
	return CWinApp::InitInstance();
}


int CTestDLLApp::Message(CString szText)
{
	if(g_pdlg)
	{
		g_pdlg->SetForegroundWindow();
//	HWND hwnd = g_pdlg->GetSafeHwnd();
//	CString h; h.Format("dll hwnd = %08x", hwnd);
//	AfxMessageBox(h);
//		::MessageBox(g_pdlg->GetSafeHwnd(), szText, "Test DLL", MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
		g_pdlg->MessageBox(szText, "Test DLL", MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
	}
	else
	{
		AfxMessageBox(szText);
	}
	return 0;
}

