#if !defined(AFX_TESTDLLDLG_H__6D060F8B_E022_4D3B_98A6_C97F23483BB4__INCLUDED_)
#define AFX_TESTDLLDLG_H__6D060F8B_E022_4D3B_98A6_C97F23483BB4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TestDLLDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestDLLDlg dialog

class CTestDLLDlg : public CDialog
{
// Construction
public:
	CTestDLLDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestDLLDlg)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestDLLDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd=NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestDLLDlg)
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDLLDLG_H__6D060F8B_E022_4D3B_98A6_C97F23483BB4__INCLUDED_)
