// DLLTest.h : main header file for the DLLTEST application
//

#if !defined(AFX_DLLTEST_H__DA75195B_6587_4044_A2FF_9A7FC3F95679__INCLUDED_)
#define AFX_DLLTEST_H__DA75195B_6587_4044_A2FF_9A7FC3F95679__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

typedef int (__stdcall* LPFNCB_SS)(char*, unsigned long); // prototype for SetStatus
typedef int (__stdcall* LPFNCB_SM)(int, char*, char*); // prototype for SendMessage
typedef int (__stdcall* LPFNCB_SLM)(unsigned long, char*, char*); // prototype for SendLogMessage


typedef int (__stdcall* LPFNDLL)(int, char*); // CommandState and SetSetting
typedef int (__stdcall* LPFNDLLF)(int, void*); // Function/Variable set pointer

//int CommandState(int nType, char* pszData=NULL);
//int SetPointer(int nType, void* pFunction);
//int SetSetting(int nType, char* pValue);



/////////////////////////////////////////////////////////////////////////////
// CDLLTestApp:
// See DLLTest.cpp for the implementation of this class
//

class CDLLTestApp : public CWinApp
{
public:
	CDLLTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDLLTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDLLTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLLTEST_H__DA75195B_6587_4044_A2FF_9A7FC3F95679__INCLUDED_)
