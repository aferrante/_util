//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DLLTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DLLTEST_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_DLL                    1000
#define IDC_BUTTON_LOADDLL              1001
#define IDC_EDIT_FUNCTION1              1002
#define IDC_BUTTON_GET1                 1003
#define IDC_EDIT_FUNCTION2              1004
#define IDC_BUTTON_GET2                 1005
#define IDC_EDIT_FUNCTION3              1006
#define IDC_BUTTON_GET3                 1007
#define IDC_STATIC_TIME                 1008
#define IDC_BUTTON_CB1                  1009
#define IDC_BUTTON_CB2                  1010
#define IDC_BUTTON_CB3                  1011
#define IDC_BUTTON_CB4                  1012
#define IDC_BUTTON_CS                   1013
#define IDC_EDIT_CSTYPE                 1014
#define IDC_EDIT_CSDATA                 1015
#define IDC_BUTTON_SS                   1016
#define IDC_EDIT_SSTYPE                 1017
#define IDC_EDIT_SSDATA                 1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
