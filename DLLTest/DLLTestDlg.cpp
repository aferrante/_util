// DLLTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DLLTest.h"
#include "DLLTestDlg.h"
#include "sys/timeb.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CDLLTestApp theApp;

_timeb g_timeb;
_timeb g_lasttimeb;

int g_SetStatus(char* pszText, unsigned long ulStatus);
int g_SendMessage(int nType, char* pszSender, char* pszMessage);
int g_SendLogMessage(unsigned long ulIcon, char* pszSender, char* pszMessage);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDLLTestDlg dialog

CDLLTestDlg::CDLLTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLLTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDLLTestDlg)
	m_szCSData = _T("");
	m_szCSType = _T("0x00000000");
	m_szDLL = _T("TestDLL.dll");
	m_szFunc1 = _T("CommandState");
	m_szFunc2 = _T("SetPointer");
	m_szFunc3 = _T("SetSetting");
	m_szSSData = _T("1");
	m_szSSType = _T("0x00000001");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hinst = NULL;
	m_dfCommandState = NULL;
	m_dfSetSetting = NULL;
	m_dfSetPointer = NULL;
}

void CDLLTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDLLTestDlg)
	DDX_Text(pDX, IDC_EDIT_CSDATA, m_szCSData);
	DDX_Text(pDX, IDC_EDIT_CSTYPE, m_szCSType);
	DDX_Text(pDX, IDC_EDIT_DLL, m_szDLL);
	DDX_Text(pDX, IDC_EDIT_FUNCTION1, m_szFunc1);
	DDX_Text(pDX, IDC_EDIT_FUNCTION2, m_szFunc2);
	DDX_Text(pDX, IDC_EDIT_FUNCTION3, m_szFunc3);
	DDX_Text(pDX, IDC_EDIT_SSDATA, m_szSSData);
	DDX_Text(pDX, IDC_EDIT_SSTYPE, m_szSSType);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDLLTestDlg, CDialog)
	//{{AFX_MSG_MAP(CDLLTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CB1, OnButtonCb1)
	ON_BN_CLICKED(IDC_BUTTON_CB2, OnButtonCb2)
	ON_BN_CLICKED(IDC_BUTTON_CB3, OnButtonCb3)
	ON_BN_CLICKED(IDC_BUTTON_CB4, OnButtonCb4)
	ON_BN_CLICKED(IDC_BUTTON_CS, OnButtonCs)
	ON_BN_CLICKED(IDC_BUTTON_GET1, OnButtonGet1)
	ON_BN_CLICKED(IDC_BUTTON_GET2, OnButtonGet2)
	ON_BN_CLICKED(IDC_BUTTON_GET3, OnButtonGet3)
	ON_BN_CLICKED(IDC_BUTTON_LOADDLL, OnButtonLoaddll)
	ON_BN_CLICKED(IDC_BUTTON_SS, OnButtonSs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDLLTestDlg message handlers

BOOL CDLLTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	_ftime(&g_timeb);
	g_lasttimeb = g_timeb;
	SetTimer(62, 50, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDLLTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDLLTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDLLTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDLLTestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	if(m_hinst) FreeLibrary(m_hinst);
	CDialog::OnCancel();
}

void CDLLTestDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent=62)
	{
		if((g_lasttimeb.time!=g_timeb.time)||(g_lasttimeb.millitm!=g_timeb.millitm))
		{
			g_lasttimeb = g_timeb;
			char pszText[256];
			char pszTime[256];
			tm* theTime = localtime( &g_timeb.time	);
			strftime( pszText, 30, "DLL set time to %H:%M:%S.", theTime );
			sprintf(pszTime, "%s%03d", pszText, g_timeb.millitm);

			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(pszTime);

		}
	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CDLLTestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

void CDLLTestDlg::OnButtonGet1() 
{
	if(!UpdateData(TRUE)) return;
	if(!m_hinst)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No DLL loaded", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	m_dfCommandState = (LPFNDLL)GetProcAddress(m_hinst, m_szFunc1);
	if(m_dfCommandState)
	{
		CString szText;
		szText.Format("Obtained function pointer for %s", m_szFunc1);
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	}
}

void CDLLTestDlg::OnButtonGet2() 
{
	if(!UpdateData(TRUE)) return;
	if(!m_hinst)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No DLL loaded", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	m_dfSetPointer = (LPFNDLLF)GetProcAddress(m_hinst, m_szFunc2);
	if(m_dfSetPointer)
	{
		CString szText;
		szText.Format("Obtained function pointer for %s", m_szFunc2);
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	}
}

void CDLLTestDlg::OnButtonGet3() 
{
	if(!UpdateData(TRUE)) return;
	if(!m_hinst)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No DLL loaded", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	m_dfSetSetting = (LPFNDLL)GetProcAddress(m_hinst, m_szFunc3);
	if(m_dfSetSetting)
	{
		CString szText;
		szText.Format("Obtained function pointer for %s", m_szFunc3);
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	}
}

void CDLLTestDlg::OnButtonLoaddll() 
{
	if(m_hinst)
	{
		FreeLibrary(m_hinst);
		m_hinst=NULL;
		m_dfCommandState = NULL;
		m_dfSetSetting = NULL;
		m_dfSetPointer = NULL;
	}
	else
	{
		if(!UpdateData(TRUE)) return;
		m_hinst = LoadLibrary(m_szDLL);
	}
	
	if(m_hinst)
	{
		GetDlgItem(IDC_BUTTON_LOADDLL)->SetWindowText("Unload DLL");
	}
	else
	{
		GetDlgItem(IDC_BUTTON_LOADDLL)->SetWindowText("Load DLL");
	}

}

void CDLLTestDlg::OnButtonCs() 
{
	if(!UpdateData(TRUE)) return;
	if(!m_dfCommandState)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for CommandState", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	unsigned long ulType = 0;
	if(m_szCSType.Find("0x")==0)
	{
		char* p = m_szCSType.GetBuffer(1) + 2;
		ulType = m_bu.xtol(p, strlen(p));
		m_szCSType.ReleaseBuffer();
	}
	else
	{
		char* p = m_szCSType.GetBuffer(1);
		ulType = m_bu.xtol(p, strlen(p));
		m_szCSType.ReleaseBuffer();
	}

	m_dfCommandState((int)ulType, m_szCSData.GetBuffer(1));
	m_szCSData.ReleaseBuffer();

}

void CDLLTestDlg::OnButtonCb1() 
{
	if(!m_dfSetPointer)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for SetPointer", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	int nReturn = m_dfSetPointer(1, (void*)g_SetStatus);

	CString szText;
	szText.Format("SetPointer returned %d", nReturn);
	::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);

}

void CDLLTestDlg::OnButtonCb2() 
{
	if(!m_dfSetPointer)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for SetPointer", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	int nReturn = m_dfSetPointer(2, (void*)g_SendMessage);

	CString szText;
	szText.Format("SetPointer returned %d", nReturn);
	::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	
}

void CDLLTestDlg::OnButtonCb3() 
{
	if(!m_dfSetPointer)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for SetPointer", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	int nReturn = m_dfSetPointer(3, (void*)g_SendLogMessage);

	CString szText;
	szText.Format("SetPointer returned %d", nReturn);
	::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	
}

void CDLLTestDlg::OnButtonCb4() 
{
	if(!m_dfSetPointer)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for SetPointer", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	int nReturn = m_dfSetPointer(4, (void*)&g_timeb);

	CString szText;
	szText.Format("SetPointer returned %d", nReturn);

	HWND hwnd = AfxGetMainWnd()->GetSafeHwnd();
	CString h; h.Format("app hwnd = %08x", hwnd);
	AfxMessageBox(h);
	::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION);
	
}

void CDLLTestDlg::OnButtonSs() 
{
	if(!UpdateData(TRUE)) return;
	if(!m_dfSetSetting)
	{
		::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), "No function address for SetSetting", "Test Application", MB_OK|MB_ICONINFORMATION);
		return;
	}
	unsigned long ulType = 0;
	if(m_szSSType.Find("0x")==0)
	{
		char* p = m_szSSType.GetBuffer(1) + 2;
		ulType = m_bu.xtol(p, strlen(p));
		m_szSSType.ReleaseBuffer();
	}
	else
	{
		char* p = m_szSSType.GetBuffer(1);
		ulType = m_bu.xtol(p, strlen(p));
		m_szSSType.ReleaseBuffer();
	}

	m_dfSetSetting((int)ulType, m_szSSData.GetBuffer(1));
	m_szSSData.ReleaseBuffer();
}


int g_SetStatus(char* pszText, unsigned long ulStatus)
{
	CString szText;
	szText.Format("SetStatus was called with status code = 0x%08x and text = %s",
		ulStatus, 
		pszText?(strlen(pszText)?pszText:"(no text)"):"(null)"
		);
	((CDLLTestDlg*)theApp.m_pMainWnd)->Message(szText);
	return 0;
}

int g_SendMessage(int nType, char* pszSender, char* pszMessage)
{
	CString szText;
	szText.Format("SendMessage was called with type code = 0x%08x, sender = %s, and message = %s",
		nType, 
		pszSender?(strlen(pszSender)?pszSender:"(no sender)"):"(null)",
		pszMessage?(strlen(pszMessage)?pszMessage:"(no message)"):"(null)"
		);
	((CDLLTestDlg*)theApp.m_pMainWnd)->Message(szText);
	return 0;
}

int g_SendLogMessage(unsigned long ulIcon, char* pszSender, char* pszMessage)
{
	CString szText;
	szText.Format("SendLogMessage was called with icon code = 0x%08x, sender = %s, and message = %s",
		ulIcon, 
		pszSender?(strlen(pszSender)?pszSender:"(no sender)"):"(null)",
		pszMessage?(strlen(pszMessage)?pszMessage:"(no message)"):"(null)"
		);
//	HWND hwnd = theApp.m_pMainWnd->GetSafeHwnd();
//	CString h; h.Format("app hwnd = %08x", hwnd);
//	AfxMessageBox(h);
	((CDLLTestDlg*)theApp.m_pMainWnd)->Message(szText);
	return 0;
}


int CDLLTestDlg::Message(CString szText)
{
	SetForegroundWindow();
//	HWND hwnd = theApp.m_pMainWnd->GetSafeHwnd();
//	CString h; h.Format("dll hwnd = %08x", hwnd);
//	AfxMessageBox(h);
//	::MessageBox(theApp.m_pMainWnd->GetSafeHwnd(), szText, "Test Application", MB_OK|MB_ICONINFORMATION|MB_SETFOREGROUND);
	MessageBox(szText, "Test Application", MB_OK|MB_ICONINFORMATION|MB_SETFOREGROUND);
	return 0;
}
