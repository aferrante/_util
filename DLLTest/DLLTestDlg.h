// DLLTestDlg.h : header file
//

#if !defined(AFX_DLLTESTDLG_H__13145E93_0246_4E75_97FC_29668E8B84DB__INCLUDED_)
#define AFX_DLLTESTDLG_H__13145E93_0246_4E75_97FC_29668E8B84DB__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\Common\TXT\BufferUtil.h"
/////////////////////////////////////////////////////////////////////////////
// CDLLTestDlg dialog

class CDLLTestDlg : public CDialog
{
// Construction
public:
	CDLLTestDlg(CWnd* pParent = NULL);	// standard constructor
	int Message(CString szText);

// Dialog Data
	//{{AFX_DATA(CDLLTestDlg)
	enum { IDD = IDD_DLLTEST_DIALOG };
	CString	m_szCSData;
	CString	m_szCSType;
	CString	m_szDLL;
	CString	m_szFunc1;
	CString	m_szFunc2;
	CString	m_szFunc3;
	CString	m_szSSData;
	CString	m_szSSType;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDLLTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	HINSTANCE m_hinst;
	LPFNDLL   m_dfCommandState;
	LPFNDLLF  m_dfSetPointer;
	LPFNDLL   m_dfSetSetting;

	CBufferUtil m_bu;


// Implementation
protected:
	HICON m_hIcon;


	// Generated message map functions
	//{{AFX_MSG(CDLLTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	afx_msg void OnButtonCb1();
	afx_msg void OnButtonCb2();
	afx_msg void OnButtonCb3();
	afx_msg void OnButtonCb4();
	afx_msg void OnButtonCs();
	afx_msg void OnButtonGet1();
	afx_msg void OnButtonGet2();
	afx_msg void OnButtonGet3();
	afx_msg void OnButtonLoaddll();
	afx_msg void OnButtonSs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLLTESTDLG_H__13145E93_0246_4E75_97FC_29668E8B84DB__INCLUDED_)
