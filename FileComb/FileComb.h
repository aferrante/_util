// FileComb.h : main header file for the FILECOMB application
//

#if !defined(AFX_FILECOMB_H__4EE659F9_8E46_4C19_8FEC_DFCDCAAE1767__INCLUDED_)
#define AFX_FILECOMB_H__4EE659F9_8E46_4C19_8FEC_DFCDCAAE1767__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFileCombApp:
// See FileComb.cpp for the implementation of this class
//

class CFileCombApp : public CWinApp
{
public:
	CFileCombApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileCombApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFileCombApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILECOMB_H__4EE659F9_8E46_4C19_8FEC_DFCDCAAE1767__INCLUDED_)
