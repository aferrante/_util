// SerialLogger.h : main header file for the SERIALLOGGER application
//

#if !defined(AFX_SERIALLOGGER_H__69498425_7C35_4F60_8EF1_1B16173D8F2E__INCLUDED_)
#define AFX_SERIALLOGGER_H__69498425_7C35_4F60_8EF1_1B16173D8F2E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSerialLoggerApp:
// See SerialLogger.cpp for the implementation of this class
//

class CSerialLoggerApp : public CWinApp
{
public:
	CSerialLoggerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialLoggerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSerialLoggerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALLOGGER_H__69498425_7C35_4F60_8EF1_1B16173D8F2E__INCLUDED_)
