; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSerialLoggerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SerialLogger.h"

ClassCount=3
Class1=CSerialLoggerApp
Class2=CSerialLoggerDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SERIALLOGGER_DIALOG

[CLS:CSerialLoggerApp]
Type=0
HeaderFile=SerialLogger.h
ImplementationFile=SerialLogger.cpp
Filter=N

[CLS:CSerialLoggerDlg]
Type=0
HeaderFile=SerialLoggerDlg.h
ImplementationFile=SerialLoggerDlg.cpp
Filter=D
LastObject=IDC_EDIT1
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=SerialLoggerDlg.h
ImplementationFile=SerialLoggerDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SERIALLOGGER_DIALOG]
Type=1
Class=CSerialLoggerDlg
ControlCount=9
Control1=IDOK,button,1208025088
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT1,edit,1350631552
Control5=IDC_CHECK_LOGGING,button,1342246915
Control6=IDC_STATIC,static,1342308352
Control7=IDC_EDIT2,edit,1350631552
Control8=IDC_EDIT3,edit,1350633600
Control9=IDC_BUTTON_CONNECT,button,1342242816

