// SerialLoggerDlg.h : header file
//

#if !defined(AFX_SERIALLOGGERDLG_H__1D76C910_0966_4ADB_85BD_9072676DF026__INCLUDED_)
#define AFX_SERIALLOGGERDLG_H__1D76C910_0966_4ADB_85BD_9072676DF026__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSerialLoggerDlg dialog

#include "..\..\Common\TTY\Serial.h"

class CSerialLoggerDlg : public CDialog
{
// Construction
public:
	CSerialLoggerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSerialLoggerDlg)
	enum { IDD = IDD_SERIALLOGGER_DIALOG };
	int		m_nCOMport;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialLoggerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	CSerial m_serial;

	// Generated message map functions
	//{{AFX_MSG(CSerialLoggerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALLOGGERDLG_H__1D76C910_0966_4ADB_85BD_9072676DF026__INCLUDED_)
