// NetClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NetClient.h"
#include "NetClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetClientDlg dialog

CNetClientDlg::CNetClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetClientDlg)
	m_bPersist = FALSE;
	m_szData = _T("<mos><roReq\\><\\mos>");
	m_szHost = _T("10.0.0.19");
	m_nSendPort = 10540;
	m_nTimerMS = 5000;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_socket = NULL;
	m_bTimerSet = false;
}

void CNetClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetClientDlg)
	DDX_Check(pDX, IDC_CHECK_PERSIST, m_bPersist);
	DDX_Text(pDX, IDC_EDIT_C_DATA, m_szData);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nSendPort);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CNetClientDlg, CDialog)
	//{{AFX_MSG_MAP(CNetClientDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_BN_CLICKED(IDC_BUTTON_C_SEND, OnButtonCSend)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetClientDlg message handlers

BOOL CNetClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CFileUtil fu;
	if(fu.GetSettings("netclient.ini", false)==	FILEUTIL_MALLOC_OK)
	{
		char* pch;
		pch = fu.GetIniString("Init", "Data", "<mos><roReq\\><\\mos>");
		if(pch)
		{
			m_szData.Format("%s", pch);
			free(pch);
		}
		pch = fu.GetIniString("Init", "Host", "10.0.0.19");
		if(pch)
		{
			m_szHost.Format("%s", pch);
			free(pch);
		}
		m_nSendPort = fu.GetIniInt("Init", "Port", 10540);
		m_nTimerMS = fu.GetIniInt("Init", "Timer", 5000);
	}
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNetClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNetClientDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNetClientDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CNetClientDlg::OnCancel() 
{
	UpdateData(TRUE);
	CFileUtil fu;
	if(fu.GetSettings("netclient.ini", false)==	FILEUTIL_MALLOC_OK)
	{
		char entry[1024];
		sprintf(entry, "%s",m_szData);

		fu.SetIniString("Init", "Data", entry);

		sprintf(entry, "%s",m_szHost);
		fu.SetIniString("Init", "Host", entry);

		fu.SetIniInt("Init", "Port", m_nSendPort);
		fu.SetIniInt("Init", "Timer", m_nTimerMS);

		// write them out
		fu.SetSettings("netclient.ini", false);
	}

	CDialog::OnCancel();
}

void CNetClientDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CNetClientDlg::OnButtonConnect() 
{
	UpdateData(TRUE);
	char host[1024];
	sprintf(host, "%s",m_szHost);
	char error[NET_ERRORSTRING_LEN];

	if(m_socket == NULL)
	{
		if(m_net.OpenConnection(host, m_nSendPort, &m_socket, 5000, 5000, error) >=NET_SUCCESS)
		{
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
			m_bPersist = TRUE;
			SetTimer(1, 50, NULL);

		}
		else
		{
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(error);
			m_bPersist = FALSE;
					KillTimer(1);

		}
	}
	else
	{
		if(m_net.CloseConnection(m_socket, error) >=NET_SUCCESS)
		{
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			m_socket = NULL;
			m_bPersist = FALSE;
					KillTimer(1);
		}
	}
	UpdateData(FALSE);
}

void CNetClientDlg::OnButtonTimer() 
{
	if(m_bTimerSet)
	{
		KillTimer(6);
		m_bTimerSet = false;

		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Set Timer");
	}
	else
	{
		UpdateData(TRUE);

		SetTimer(6, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
		m_bTimerSet = true;
	}
	
}

void CNetClientDlg::OnButtonCSend() 
{
	CWaitCursor cw;
	UpdateData(TRUE);

	CNetData data;

	data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
	data.m_ucCmd = m_nCmd&0xff;       // the command byte
	data.m_ucSubCmd = m_nSubCmd&0xff;       // the subcommand byte

	if(m_nSubCmd>0) data.m_ucType |= NET_TYPE_HASSUBC; 

	
	data.m_pucData = NULL;
	data.m_ulDataLen = m_szData.GetLength();
	if(data.m_ulDataLen>0)
	{
		char* pch = (char*)malloc(data.m_ulDataLen+1);     // pointer to the payload data
		if(pch)
		{
			strcpy(pch, m_szData);
			data.m_pucData = (unsigned char*)pch;
		}
	}
	char buffer[NET_ERRORSTRING_LEN];


	char host[256];

			_snprintf(buffer, NET_ERRORSTRING_LEN-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
				clock(),
					data.m_ucType,
					data.m_ucCmd,
					data.m_ucSubCmd,
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);
			GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

	if(m_bPersist)	
	{
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		char* pch = strstr(m_chConns, host);
		if(pch==NULL)  // first time.
		{
			strcpy(host, m_szHost);
			nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer);
			if(nreturn<NET_SUCCESS)
			{
				char snarf[2000];
				sprintf(snarf, "Persist open: %s", buffer);
				AfxMessageBox(snarf);
//				AfxMessageBox(m_chConns);
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.

			}
			else
			{
				sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);
				strcat(m_chConns, host);
				sprintf(host, "%ld]", s);
				strcat(m_chConns, host);

//				AfxMessageBox(m_chConns);
			}
		}
		else
		{
			s = atol(pch+strlen(host));
			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
		}




		if(nreturn>=NET_SUCCESS)
		{
			unsigned long ulTime = clock();
			if(m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, buffer)<NET_SUCCESS)
			{
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
				if(data.m_ucCmd != NET_CMD_NAK)  // check the comm error if NAK didnt come thru ok
				{
					char snarf[2000];
					sprintf(snarf, "Persist send: %s", buffer);
					AfxMessageBox(snarf);
					// no need to nak back
					//m_net.SendData(&data, s, 5000, 0, NET_SND_NAK|NET_SND_NO_RX, buffer);
				}
			}
			else
			{
				unsigned long ulTime2 = clock();
				// ack back
				m_net.SendData(&data, s, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT, buffer);

				_snprintf(buffer, NET_ERRORSTRING_LEN-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms\r\n0x%08x", 
					clock(),
						data.m_ucType,
						data.m_ucCmd,
						data.m_ucSubCmd,
						data.m_ulDataLen,
						data.m_pucData==NULL?"":(char*)data.m_pucData		,
						ulTime2 - ulTime,
						clock() - ulTime2,
						clock() - ulTime,
						NET_RCV_EOLN|EOLN_HTTP
					);
				GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

			}
		}
	}
	else
	{

		// lets make sure theres no persistent connection now. if there is use it first to get rid of the conn,
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		bool bDisconn = false;
		char* pch = strstr(m_chConns, host);
		if(pch!=NULL)  // it exists.
		{
			s = atol(pch+strlen(host));
//			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
			bDisconn = true;

			//now have to remove it.

			int nLen = strlen(m_chConns);
			char* pch2 = strstr(pch, "]");
			memcpy(pch, pch2+1, (m_chConns+nLen) - pch2);

		}


		strcpy(host, m_szHost);
			_snprintf(buffer, NET_ERRORSTRING_LEN-1, "sending: %d: checksum:%d, type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
				clock(),
				data.m_ulDataLen?(m_net.Checksum(data.m_pucData, data.m_ulDataLen)+data.m_ucType+data.m_ucCmd+data.m_ucSubCmd):(data.m_ucType+data.m_ucCmd+data.m_ucSubCmd),
					data.m_ucType,
					data.m_ucCmd,
					data.m_ucSubCmd,
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);
			GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

//			AfxMessageBox("sending");

			unsigned long ulTime = clock();
		if(bDisconn)
			nreturn = m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR, buffer);
		else
			nreturn = m_net.SendData(&data, host, m_nSendPort, 5000, 0, NET_SND_CMDTOSVR, &s, buffer);

		if(nreturn<NET_SUCCESS)
		{
//			AfxMessageBox("failure");
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
			
			if(data.m_ucCmd != NET_CMD_NAK) // means we received a neg reply, so comm ok, but it doesnt expect a reply.
			{
				char snarf[2000];
				sprintf(snarf, "Transient send: %s", buffer);
				AfxMessageBox(snarf);
			}
		}
		else
		{
//			AfxMessageBox("success");

				unsigned long ulTime2 = clock();
			// send the ack
			m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK, buffer);

				_snprintf(buffer, NET_ERRORSTRING_LEN-1, "success %d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms", 
					clock(),
						data.m_ucType,
						data.m_ucCmd,
						data.m_ucSubCmd,
						data.m_ulDataLen,
						data.m_pucData==NULL?"":(char*)data.m_pucData		,
						ulTime2 - ulTime,
						clock() - ulTime2,
						clock() - ulTime
					);
			GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
		}

		m_net.CloseConnection(s);
	}
	
}

void CNetClientDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 6)
	{ 
		OnButtonCSend();
	}
	else
	CDialog::OnTimer(nIDEvent);
}
