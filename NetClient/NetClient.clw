; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNetClientDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "NetClient.h"

ClassCount=3
Class1=CNetClientApp
Class2=CNetClientDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_NETCLIENT_DIALOG

[CLS:CNetClientApp]
Type=0
HeaderFile=NetClient.h
ImplementationFile=NetClient.cpp
Filter=N

[CLS:CNetClientDlg]
Type=0
HeaderFile=NetClientDlg.h
ImplementationFile=NetClientDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CNetClientDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=NetClientDlg.h
ImplementationFile=NetClientDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_NETCLIENT_DIALOG]
Type=1
Class=CNetClientDlg
ControlCount=15
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_C_SEND,button,1342242816
Control4=IDC_EDIT_C_REPLY,edit,1350633668
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_C_DATA,edit,1350635652
Control7=IDC_EDIT_HOST,edit,1350631552
Control8=IDC_STATIC,static,1342308352
Control9=IDC_EDIT_C_PORT,edit,1350631552
Control10=IDC_STATIC,static,1342308352
Control11=IDC_CHECK_PERSIST,button,1208025091
Control12=IDC_BUTTON_TIMER,button,1342242816
Control13=IDC_EDIT_TIMERMS,edit,1350639744
Control14=IDC_BUTTON_CONNECT,button,1342242816
Control15=IDC_STATIC,static,1342308352

