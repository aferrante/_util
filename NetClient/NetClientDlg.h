// NetClientDlg.h : header file
//

#if !defined(AFX_NETCLIENTDLG_H__D0B8C7ED_E7F0_425C_9924_7894E13533F6__INCLUDED_)
#define AFX_NETCLIENTDLG_H__D0B8C7ED_E7F0_425C_9924_7894E13533F6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\Common\LAN\NetUtil.h"
#include "..\..\Common\TXT\FileUtil.h"
/////////////////////////////////////////////////////////////////////////////
// CNetClientDlg dialog

class CNetClientDlg : public CDialog
{
// Construction
public:
	CNetClientDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNetClientDlg)
	enum { IDD = IDD_NETCLIENT_DIALOG };
	BOOL	m_bPersist;
	CString	m_szData;
	CString	m_szHost;
	int		m_nSendPort;
	int		m_nTimerMS;
	//}}AFX_DATA


	CNetUtil m_net;
	SOCKET m_socket;
	bool m_bTimerSet;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetClientDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CNetClientDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonTimer();
	afx_msg void OnButtonCSend();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETCLIENTDLG_H__D0B8C7ED_E7F0_425C_9924_7894E13533F6__INCLUDED_)
