//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by NetClient.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_NETCLIENT_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_CONNECT              1000
#define IDC_BUTTON_TIMER                1021
#define IDC_EDIT_TIMERMS                1022
#define IDC_BUTTON_C_SEND               1056
#define IDC_EDIT_C_REPLY                1057
#define IDC_EDIT_C_DATA                 1060
#define IDC_EDIT_HOST                   1061
#define IDC_EDIT_C_PORT                 1062
#define IDC_CHECK_PERSIST               1063

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
